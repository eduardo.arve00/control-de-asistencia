-- MySQL dump 10.13  Distrib 5.5.20, for Win32 (x86)
--
-- Host: localhost    Database: personal
-- ------------------------------------------------------
-- Server version	5.5.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asistencia`
--

DROP TABLE IF EXISTS `asistencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asistencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empleado` int(11) NOT NULL,
  `estatus` varchar(3) DEFAULT NULL,
  `nombre_completo` varchar(200) DEFAULT NULL,
  `tipo` varchar(3) DEFAULT NULL,
  `cedula` varchar(16) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora_sal` time DEFAULT NULL,
  `hora_ing` time DEFAULT NULL,
  `comentario` varchar(200) DEFAULT NULL,
  `id_m_horario` int(11) NOT NULL,
  `min_extras` int(11) DEFAULT '0',
  `min_retrazo` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_id_empleado_idx` (`id_empleado`),
  KEY `fk_id_horario_idx` (`id_m_horario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asistencia`
--

LOCK TABLES `asistencia` WRITE;
/*!40000 ALTER TABLE `asistencia` DISABLE KEYS */;
INSERT INTO `asistencia` VALUES (1,1,NULL,'EDUARDO, ARVELAIZ',NULL,'V-19709183','2014-11-04','13:04:12','13:04:05',NULL,1,14,258),(2,1,NULL,'NUEVO, EMPLEADO',NULL,'V-11111111','2014-11-05','18:45:25','18:45:19',NULL,1,355,600);
/*!40000 ALTER TABLE `asistencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `eliminado` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_reg` datetime DEFAULT NULL,
  `id_m_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_usuario_idx` (`id_m_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'PROGRAMADOR','PROGRAMADOR DE SOFTWARE',1,NULL,'2014-11-04 12:19:21','2014-11-04 12:19:21',1);
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '0',
  `eliminado` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_reg` datetime DEFAULT NULL,
  `id_m_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_usuario_idx` (`id_m_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` VALUES (1,'DESARROLLO','DEPARTAMENTO DE DESARROLLO DE SOFTWARE',1,NULL,'2014-11-04 12:18:57','2014-11-04 12:18:57',1);
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(16) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `nombres` varchar(200) DEFAULT NULL,
  `estatus` varchar(3) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT NULL,
  `huella` varbinary(1024) DEFAULT NULL,
  `id_m_usuario` int(11) DEFAULT NULL,
  `id_m_cargo` int(11) NOT NULL,
  `id_m_departamentos` int(11) NOT NULL,
  `fecha_reg` date DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `dir_hab` varchar(400) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `edo_civil` int(11) DEFAULT NULL,
  `nacionalidad` int(11) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `id_m_empleado` int(11) NOT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `modalidad` varchar(50) DEFAULT NULL,
  `modalidad_des` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_cargo_idx` (`id_m_cargo`),
  KEY `fk_id_departamentos_idx` (`id_m_departamentos`),
  KEY `fk_id_usuario_idx` (`id_m_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'V-19709183','ARVELAIZ','EDUARDO',NULL,NULL,NULL,1,1,1,'2014-11-04',NULL,'TUCUPIDO','0426-9302733','1989-11-18',0,0,'2014-11-04',0,'','FIJO',''),(2,'V-11111111','EMPLEADO','NUEVO',NULL,NULL,NULL,1,1,1,'2014-11-05',NULL,'115151515151515','0426-9999999','2014-11-05',0,0,'2014-11-05',0,'asdasdasd','CONTRATADO','');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `entrada` time DEFAULT NULL,
  `HETolerancia` time DEFAULT NULL,
  `HEalmuerzo` time DEFAULT NULL,
  `HSAlmuerzo` time DEFAULT NULL,
  `salida` time DEFAULT NULL,
  `HSTolerancia` time DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_reg` datetime DEFAULT NULL,
  `tipo` varchar(1) DEFAULT NULL,
  `id_m_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_usuario_idx` (`id_m_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
INSERT INTO `horario` VALUES (1,'TEST','TEST','08:30:17','08:45:18','08:07:19','08:07:20','12:00:22','12:50:00',1,0,'2014-10-21 08:54:32','2014-10-17 08:07:36','1',0),(2,'TESTH','TEST2','21:00:00','21:00:00',NULL,NULL,'21:00:00','21:00:00',1,NULL,'2014-10-21 08:57:02','2014-10-21 08:56:20',NULL,1),(3,'ADMINISTRATIVO','HORARIO PARA PERSONAL ADMINISTRATIVO','08:00:00','08:15:00',NULL,NULL,'12:00:00','12:30:00',1,NULL,'2014-10-23 14:49:54','2014-10-23 14:49:54',NULL,1);
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario_emp`
--

DROP TABLE IF EXISTS `horario_emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario_emp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_m_empleado` int(11) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT NULL,
  `lunes` int(11) DEFAULT NULL,
  `martes` int(11) DEFAULT NULL,
  `miercoles` int(11) DEFAULT NULL,
  `jueves` int(11) DEFAULT NULL,
  `viernes` int(11) DEFAULT NULL,
  `sabado` int(11) DEFAULT NULL,
  `domingo` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_reg` datetime DEFAULT NULL,
  `fecha_venc` date DEFAULT NULL,
  `id_m_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_empleado_idx` (`id_m_empleado`),
  KEY `fk_id_usuario_idx` (`id_m_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario_emp`
--

LOCK TABLES `horario_emp` WRITE;
/*!40000 ALTER TABLE `horario_emp` DISABLE KEYS */;
INSERT INTO `horario_emp` VALUES (1,1,'',0,NULL,1,1,1,1,1,1,1,'2014-11-04 12:46:05','2014-11-04 12:46:05',NULL,1);
/*!40000 ALTER TABLE `horario_emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametros`
--

DROP TABLE IF EXISTS `parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(50) DEFAULT NULL,
  `valor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='tabla de parametros';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametros`
--

LOCK TABLES `parametros` WRITE;
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;
INSERT INTO `parametros` VALUES (1,'NOMBRE_DIRECTOR','JOSE CAMACHO'),(2,'CEDULA_DIRECTOR','V-66666666');
/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permisos`
--

DROP TABLE IF EXISTS `permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisos`
--

LOCK TABLES `permisos` WRITE;
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` VALUES (1,'REPOSO MEDICO','REPOSO POR CONDICION MEDICA',1,NULL),(2,'PERMISO DIARIO','PERMISO POR UN DIA',1,NULL);
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permisos_emp`
--

DROP TABLE IF EXISTS `permisos_emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permisos_emp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_permiso` int(11) DEFAULT NULL,
  `id_empleado` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisos_emp`
--

LOCK TABLES `permisos_emp` WRITE;
/*!40000 ALTER TABLE `permisos_emp` DISABLE KEYS */;
/*!40000 ALTER TABLE `permisos_emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `nombres` varchar(200) DEFAULT NULL,
  `apellidos` varchar(200) DEFAULT NULL,
  `clave` varchar(200) DEFAULT NULL,
  `estatus` tinyint(1) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `fecha_reg` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clave_UNIQUE` (`clave`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'ADMIN','PEDRO','PEREZ','ADMIN',1,0,'2014-09-23'),(2,'TEST','TEST','TEST','TEST',1,1,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-06 12:34:26
