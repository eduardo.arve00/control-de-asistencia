/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.Reportes;

import controldeasistencias.utils.Boot;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.log4j.LogManager;
import controldeasistencias.utils.LogUtility;
import java.util.HashMap;

/**
 *
 * @author Administrador
 */
public class GenerarReportes {

    static Connection conn = null;

    static {
        // Cargamos el driver JDBC
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded...");
            org.apache.log4j.Logger log = LogManager.getLogger(GenerarReportes.class);
        } catch (ClassNotFoundException e) {
            System.out.println("MySQL JDBC Driver not found.");
            System.exit(1);
        }
        //Para iniciar el Logger.
        //inicializaLogger();
        try {
            //obtiene las propiedades de conexion desde un singleton
            String url = (String) Boot.getProperties().get(Boot.KEY_PARAM.keyURL); 
            String user = (String) Boot.getProperties().get(Boot.KEY_PARAM.keyUser); 
            String pass = (String) Boot.getProperties().get(Boot.KEY_PARAM.keyPass); 
            //REALIZA LAS CONEXION CON LA BASE DE DATOS.
            conn = DriverManager.getConnection(url, user, pass);
            
            conn.setAutoCommit(false);
            System.out.println("Conexion establecida");
        } catch (SQLException e) {
            System.out.println("Error de conexión: " + e.getMessage());
            System.exit(4);
        }

    }

    public static void preCompilar(String reporte) {
        try {
            JasperReport report = JasperCompileManager.compileReport("reportes//"
                    + reporte + ".jrxml");
        } catch (JRException ex) {
            Logger.getLogger(GenerarReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void Generar(String reporte, HashMap parametros) {
        try {
            JasperReport report = JasperCompileManager.compileReport("reportes//"
                    + reporte + ".jrxml");

            JasperPrint print = JasperFillManager.fillReport(report, parametros, conn);

            JasperExportManager.exportReportToPdfFile(print, reporte + ".pdf");

            //Para visualizar el pdf directamente desde java
            JasperViewer.viewReport(print, false);

        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, "No se puede generar el reporte",
                    "Error", JOptionPane.ERROR_MESSAGE);
            LogUtility.writeToErrorLog(ex.getMessage());
        }

    }

    public static void main(String args[]) {
        System.out.println("PreCompilando");
        GenerarReportes.preCompilar("ListaEmpleados");
        System.out.println("compilando");
        GenerarReportes.Generar("ListaEmpleados", null);
    }
}
