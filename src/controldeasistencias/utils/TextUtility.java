/**
 * ************************************************************************
 *
 * JHTML - Cross Platform HTML Design Environment Copyright (C) 2000 Riyad Kalla
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Created: 6/14/2000 File Name: TextUtility.java File Desc: Handles text file
 * related operations. Responsibilities: Controls all loading/unloading of flat
 * text files for parsing.
 *
 *************************************************************************
 */
package utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;

public class TextUtility {

    private BufferedReader fileReader = null;
    private BufferedWriter fileWriter = null;

    public TextUtility(String absFileName) {
        try {
            fileReader = new BufferedReader(new FileReader(absFileName));
        } catch (FileNotFoundException e) {
            File createNewFile = new File(absFileName);

            try {
                createNewFile.createNewFile();
                fileReader = new BufferedReader(new FileReader(absFileName));
            } catch (Exception ee) {
                System.err.println(ee);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public TextUtility(String absFileName, boolean append) {
        try {
            fileWriter = new BufferedWriter(new FileWriter(absFileName, append));
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void closeFile() {
        if (fileWriter == null) {
            try {
                fileReader.close();
            } catch (Exception e) {
                System.err.println(e);
            }
        } else if (fileReader == null) {
            try {
                fileWriter.close();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

    public boolean isReady() {
        boolean isReady = false;

        try {
            isReady = fileReader.ready();
        } catch (Exception e) {
            System.err.println(e);
        }

        return isReady;
    }

    public String readLine() {
        String lineText = null;

        try {
            lineText = fileReader.readLine();
        } catch (Exception e) {
            System.err.println(e);
        }

        return lineText;
    }

    public void writeLine(String lineText) {
        try {
            fileWriter.write(lineText + System.getProperty("line.separator"));
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void flush() {
        try {
            fileWriter.flush();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
