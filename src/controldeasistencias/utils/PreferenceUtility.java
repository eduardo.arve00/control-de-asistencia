/**
 * ************************************************************************
 *
 * JHTML - Cross Platform HTML Design Environment Copyright (C) 2000 Riyad Kalla
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Created: 6/14/2000 File Name: PreferenceUtility.java File Desc: Handles
 * preference requests for JHTML. Responsibilities: Loads preferences from
 * jhtmlprefs.conf file as well as handling requests for preferences and setting
 * new preferences ( from preference menu ).
 *
 *************************************************************************
 */
package controldeasistencias.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.Properties;
import java.util.Enumeration;

public class PreferenceUtility {

    private String preferenceFileName = null;
    private Properties jhtmlPreferences = new Properties();

    private static PreferenceUtility startupPreferences = new PreferenceUtility();
    private static PreferenceUtility logPreferences = new PreferenceUtility();
    private static PreferenceUtility editorPreferences = new PreferenceUtility();
    private static PreferenceUtility lookAndFeelPreferences = new PreferenceUtility();
    private static PreferenceUtility windowPreferences = new PreferenceUtility();
    public static String BaseDir = "";

    public static boolean initPreferenceUtility(String Base) {
        BaseDir = Base;
        return startupPreferences.loadPreferences("startup.cfg") && logPreferences.loadPreferences("log.cfg") && editorPreferences.loadPreferences("editor.cfg") && lookAndFeelPreferences.loadPreferences("lookandfeel.cfg") && windowPreferences.loadPreferences("window.cfg");
    }

    public boolean loadPreferences(String preferenceFileName) {
        this.preferenceFileName = preferenceFileName;
        try {
            //BufferedInputStream preferencesIn = new BufferedInputStream( new FileInputStream( "net/sourceforge/jhtml/config/" + this.preferenceFileName ) );
            BufferedInputStream preferencesIn = new BufferedInputStream(new FileInputStream(BaseDir + "config/" + this.preferenceFileName));

            jhtmlPreferences.load(preferencesIn);
            preferencesIn.close();
        } catch (Exception e) {
            System.err.println(e);
            return false;
        }
        System.out.println("Preferencias Cargadas: " + preferenceFileName);
        return true;
    }

    public void storePreferences() {
        try {
            BufferedOutputStream preferencesOut = new BufferedOutputStream(new FileOutputStream(BaseDir + "config/" + preferenceFileName));
            jhtmlPreferences.store(preferencesOut, ("Changes to " + preferenceFileName + " are being saved."));
            preferencesOut.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public String getProperty(String propertyKey) {
        return jhtmlPreferences.getProperty(propertyKey);
    }

    public Enumeration getPropertyNames() {
        return jhtmlPreferences.propertyNames();
    }

    public void setProperty(String propertyKey, String propertyValue) {
        jhtmlPreferences.setProperty(propertyKey, propertyValue);
    }

    public int size() {
        return jhtmlPreferences.size();
    }

    public static PreferenceUtility getStartupPreferences() {
        return startupPreferences;
    }

    public static PreferenceUtility getLogPreferences() {
        return logPreferences;
    }

    public static PreferenceUtility getEditorPreferences() {
        return editorPreferences;
    }

    public static PreferenceUtility getLookAndFeelPreferences() {
        return lookAndFeelPreferences;
    }

    public static PreferenceUtility getWindowPreferences() {
        return windowPreferences;
    }

    public static void storeAllPreferences() {
        startupPreferences.storePreferences();
        logPreferences.storePreferences();
        editorPreferences.storePreferences();
        lookAndFeelPreferences.storePreferences();
        windowPreferences.storePreferences();
    }
}
