/**************************************************************************

JHTML - Cross Platform HTML Design Environment
Copyright (C) 2000  Riyad Kalla

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


Created: 7/6/2000
File Name: ScreenUtility.java
File Desc: Used to access client screen properties.
Responsibilities:
	Provides information about the clients screen to inquiring components.

**************************************************************************/

package controldeasistencias.utils.splashscreen;

import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Dimension;

public class ScreenUtility extends Object
{
    private static int screenWidth = 0;
    private static int screenHeight = 0;
    
    public static Point getCenterCoordinates( int width, int height )
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screenWidth = (int)screenSize.getWidth();
        screenHeight = (int)screenSize.getHeight();
        
        return new Point( ( screenWidth / 2 ) - ( width / 2 ), ( screenHeight / 2 ) - ( height / 2 ) );
    }
    
    public static Dimension getDimensionScreen () {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }
}