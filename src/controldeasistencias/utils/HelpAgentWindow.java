/**************************************************************************

JHTML - Cross Platform HTML Design Environment
Copyright (C) 2000  Riyad Kalla

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


Created: 7/24/2000
File Name: HelpAgent.java
File Desc: Help Agent.
Responsibilities:
	Help agent can be spawned as a docked window inside JHTML or a
	seperate window. The Help Agent attempts to be a friendly
	help interface for learning JHTML and hopefully basic HTML.

**************************************************************************/

package controldeasistencias.utils;

import controldeasistencias.utils.splashscreen.ScreenUtility;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;


public class HelpAgentWindow extends Object
{
	private File currentDirectory = new File( "" );
	private JLabel locationName = new JLabel( " " );
	private JFrame helpAgentWindow = new JFrame( "Ayuda" );
	private JButton backButton = null;
	private JButton forwardButton = null;
	private JButton homeButton = null;
	private JButton refreshButton = null;
	private JButton analyzeButton = null;
	private JEditorPane editorPane = new JEditorPane();
	private int urlPosition = -1;
	private Vector urlHistory = new Vector();
	private Container contentPane = helpAgentWindow.getContentPane();
	
	public HelpAgentWindow()
	{
		initHelpAgentWindow();
	}
	
	public void toggleVisibility()
	{
		if( helpAgentWindow.isVisible() )
			helpAgentWindow.hide();
		else
			helpAgentWindow.show();
	}
	
	public int getWidth()
	{
		return helpAgentWindow.getWidth();
	}
	
	public int getHeight()
	{
		return helpAgentWindow.getHeight();
	}
	
	public Point getLocation()
	{
		return helpAgentWindow.getLocation();
	}
	
	
	public void setPage( URL location )
	{
		helpAgentWindow.show();
		
		try
		{
			editorPane.setPage( location );
		}
		catch( Exception e )
		{
			System.err.println( e );
		}
	}
	
	public void setPage( String page )
	{
		helpAgentWindow.show();
		
		try
		{
			editorPane.setPage( "file:" + PreferenceUtility.BaseDir + "html/" + page );
		}
		catch( Exception e )
		{
			System.err.println( e );
		}
	}
	
	public void refreshPage()
	{
		URL pageToRefresh = editorPane.getPage();
		
		setPage( "refresh.html" );
		setPage( pageToRefresh );
	}
	
	private void back()
	{
		if( urlPosition > 0 )
		{
			urlPosition--;
			setPage( (URL)urlHistory.elementAt( urlPosition ) );
		}
	}
	
	private void forward()
	{
		if( urlPosition < ( urlHistory.size() - 1 ) )
		{
			urlPosition++;
			setPage( (URL)urlHistory.elementAt( urlPosition ) );
		}
	}
	
	private void setHomePage()
	{
		try
		{
			editorPane.setPage( "file:" + PreferenceUtility.BaseDir + "html/home.html" );
			urlHistory.add( editorPane.getPage() );
			urlPosition = urlHistory.size() - 1;
			locationName.setText( editorPane.getPage().getFile() );
		}
		catch( Exception e )
		{
			System.err.println( e );
		}
	}
	
	private void initHelpAgentWindow()
	{
		helpAgentWindow.setSize(480, 620);
		
		
		Icon backButton_Icon = null;
		Icon forwardButton_Icon = null;
		Icon homeButton_Icon = null;
		Icon refreshButton_Icon = null;
		Icon analyzeButton_Icon = null;
		
		if( Boolean.valueOf( PreferenceUtility.getLookAndFeelPreferences().getProperty( "helpAgentBigIcons" ) ).booleanValue() )
		{
			backButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/navigation/Back24.gif" );
			forwardButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/navigation/Forward24.gif" );
			homeButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/navigation/Home24.gif" );
			refreshButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/general/Refresh24.gif" );
			analyzeButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/general/ContextualHelp24.gif" );
		}
		else
		{
			backButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/navigation/Back16.gif" );
			forwardButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/navigation/Forward16.gif" );
			homeButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/navigation/Home16.gif" );
			refreshButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/general/Refresh16.gif" );
			analyzeButton_Icon = new ImageIcon( PreferenceUtility.BaseDir+"resources/graphics/icons/general/ContextualHelp16.gif" );
		}
		
		backButton = new JButton( "Atras", backButton_Icon );
		forwardButton = new JButton( "Siguiente", forwardButton_Icon );
		homeButton = new JButton( "Inicio", homeButton_Icon );
		refreshButton = new JButton( "Actualizar", refreshButton_Icon );
		analyzeButton = new JButton( "Analizar", analyzeButton_Icon );
		
		backButton.setToolTipText( "Go backward to a previously visited document" );
		forwardButton.setToolTipText( "Go forward to a previously visited document" );
		homeButton.setToolTipText( "Go to the home page" );
		refreshButton.setToolTipText( "Reload the current page" );
		analyzeButton.setToolTipText( "Click on a component in the JHTML window to jump to help based on it" );
		
		backButton.addActionListener( new ButtonListener() );
		forwardButton.addActionListener( new ButtonListener() );
		homeButton.addActionListener( new ButtonListener() );
		refreshButton.addActionListener( new ButtonListener() );
		analyzeButton.addActionListener( new ButtonListener() );
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add( backButton );
		buttonPanel.add( forwardButton );
		buttonPanel.add( homeButton );
		buttonPanel.add( refreshButton );
		//buttonPanel.add( analyzeButton );
		
		JPanel middleContainer = new JPanel();
		middleContainer.setBackground( Color.white );
		middleContainer.setBorder( BorderFactory.createLineBorder( Color.black ) );
		middleContainer.add( editorPane );
		
		contentPane.setLayout( new BorderLayout() );
		contentPane.add( buttonPanel, BorderLayout.NORTH );
		contentPane.add( new JScrollPane( middleContainer ), BorderLayout.CENTER );
		contentPane.add( locationName, BorderLayout.SOUTH );
		
		editorPane.setEditable( false );
		editorPane.addHyperlinkListener( new HyperlinkEventListener() );
		setHomePage();
	}
	
	private class ButtonListener implements ActionListener
	{
		public void actionPerformed( ActionEvent evt )
		{
			if( evt.getSource() == backButton )
			{
				back();
			}
			else if( evt.getSource() == forwardButton )
			{
				forward();
			}
			else if( evt.getSource() == homeButton )
			{
				setHomePage();
			}
			else if( evt.getSource() == refreshButton )
			{
				refreshPage();
			}
			
		}
	}
	
	private class HyperlinkEventListener implements HyperlinkListener
	{
		public void hyperlinkUpdate( HyperlinkEvent evt )
		{
			if( evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED )
			{
				urlHistory.add( evt.getURL() );
				urlPosition = urlHistory.size() - 1;
				setPage( evt.getURL() );
			}
		}
	}
	
}