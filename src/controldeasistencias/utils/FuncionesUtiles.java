/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.SwingConstants;
import utils.NumberToLetterConverter;

/**
 *
 * @author Administrador
 */
public class FuncionesUtiles {
    public static String getDateToString (Date date) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
        return formatoDeFecha.format(date); 
    }
    
    public static String getDateToStringWhitSecongs(Date date) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatoDeFecha.format(date); 
    }
    
     public static String getDateToBackUp(Date date) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
        return formatoDeFecha.format(date); 
    }
    
    
    public static String getCurrentTime(Date date) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("HH:mm:ss");
        return formatoDeFecha.format(date); 
    }
    
    public static String getTimeHHmm(Date date) {
        if (date == null) return " ";
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("HH:mm");
        return formatoDeFecha.format(date); 
    }
    
    public static String getMes(Date date) {
        if (date == null) return " ";
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("MMMMM");
        return formatoDeFecha.format(date); 
   
    }
    
    public static String fechaEnLetras (Date date) {
        Calendar calendar = Calendar.getInstance();
        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        int anio = calendar.get(Calendar.YEAR);
        String strDia = NumberToLetterConverter.convertNumberToLetter(String.valueOf(dia)).replace("BOLIVARES", " ").toLowerCase();
        return strDia + "(" + dia + ") dias del mes de " + getMes(calendar.getTime()) + " del " + anio ;
    }
    
    public static void main (String args[]) {
        System.out.println(FuncionesUtiles.fechaEnLetras(new Date()));
       
    }
}
