/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.utils.View;

import javax.swing.JSpinner;

/**
 *
 * @author Administrador
 */
public class JTimeComponent extends JSpinner {
	
	public JTimeComponent () {
		this.setEditor (new JSpinner.DateEditor (this, "HH:mm"));
	}

}
