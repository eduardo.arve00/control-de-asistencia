/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.utils.View;

import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

/**
 *
 * @author Administrador
 */
public class JTimeEditor extends JSpinner.DateEditor {

    public JTimeEditor(JSpinner spinner) {
       
        super(spinner, "HH:mm");
         
    }
    
}
