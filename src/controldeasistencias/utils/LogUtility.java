/**
 * ************************************************************************
 *
 * JHTML - Cross Platform HTML Design Environment Copyright (C) 2000 Riyad Kalla
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Created: 6/14/2000 File Name: LogUtility.java File Desc: Writes messages out
 * to log files. Responsibilities: To recieve and write <code>String</code>
 * messages out to jhtmlruntime.log and jhtmlerror.log files.
 *
 *************************************************************************
 */
package controldeasistencias.utils;

import java.util.Date;
import utils.TextUtility;


public class LogUtility {

    private static boolean logErrorsToFile = false;
    private static boolean printErrorsToConsole = false;
    private static boolean logRuntimeToFile = false;
    private static boolean printRuntimeToConsole = false;
    private static TextUtility errorLog = null;
    private static TextUtility runtimeLog = null;
    private static final String errorLogPath = PreferenceUtility.BaseDir + "logs/error.log";
    private static final String runtimeLogPath = PreferenceUtility.BaseDir + "logs/runtime.log";

    public static boolean initLogUtility() {
        PreferenceUtility logPreferences = new PreferenceUtility();
        logPreferences.loadPreferences("log.cfg");

        if (logPreferences != null) {
            logErrorsToFile = Boolean.valueOf(logPreferences.getProperty("logErrorsToFile")).booleanValue();
            printErrorsToConsole = Boolean.valueOf(logPreferences.getProperty("printErrorsToConsole")).booleanValue();
            logRuntimeToFile = Boolean.valueOf(logPreferences.getProperty("logRuntimeToFile")).booleanValue();
            printRuntimeToConsole = Boolean.valueOf(logPreferences.getProperty("printRuntimeToConsole")).booleanValue();
        }

        errorLog = new TextUtility(errorLogPath, true);
        runtimeLog = new TextUtility(runtimeLogPath, true);

        if ((errorLog != null) && (runtimeLog != null)) {
            return true;
        }

        return false;
    }

    public static void writeToErrorLog(String errorMessage) {
        if (logErrorsToFile) {
            errorLog.writeLine("Error Log: [" + new Date() + "]: " + errorMessage);
            errorLog.flush();
        }

        if (printErrorsToConsole) {
            System.err.println("Error Log: [" + new Date() + "]: " + errorMessage);
        }
    }

    public static void writeToRuntimeLog(String runtimeMessage) {
        if (logRuntimeToFile) {
            runtimeLog.writeLine("Runtime Log: [" + new Date() + "]: " + runtimeMessage);
            runtimeLog.flush();
        }

        if (printRuntimeToConsole) {
            System.err.println("Runtime Log: [" + new Date() + "]: " + runtimeMessage);
        }
    }

    public static void clearLogs() {
        errorLog = new TextUtility(errorLogPath, false);
        runtimeLog = new TextUtility(runtimeLogPath, false);

        initLogUtility();
    }
}
