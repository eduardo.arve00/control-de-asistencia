/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.swing.JOptionPane;
import persistence.util.JpaUtil;

/**
 *
 * @author Administrador
 */
public class Boot {

    public interface KEY_PARAM {

        String keyURL = "javax.persistence.jdbc.url";
        String keyUser = "javax.persistence.jdbc.user";
        String keyPass = "javax.persistence.jdbc.password";
        
    }
    
    private EncryptedProperties prop;
    private EntityManagerFactory emf;
    private static Map<String, Object> properties;
    private static Boot instance;

    private Boot() {
        try {
            prop = new EncryptedProperties("xProperti");

        } catch (Exception ex) {
            Logger.getLogger(Boot.class.getName()).log(Level.SEVERE, null, ex);
        }

        InputStream input = null;

        try {
            File f = new File("myProperties");
            if (f.exists()) {
                FileInputStream in = new FileInputStream(f);
                prop.load(in);

                String valUser = prop.getProperty("user");
                String valPass = prop.getProperty("pass");
                String valUrl = "jdbc:mysql://" + prop.getProperty("serv") + "/" + prop.getProperty("bd");

                properties = new HashMap();
                properties.put(KEY_PARAM.keyURL, valUrl);
                properties.put(KEY_PARAM.keyUser, valUser);
                properties.put(KEY_PARAM.keyPass, valPass);

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BDConfig.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BDConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Map<String, Object> getProperties() {
        if (null == instance) {
            System.out.println("Iniciando Boot");
            instance = new Boot();
        }
        return properties;
    }
}
