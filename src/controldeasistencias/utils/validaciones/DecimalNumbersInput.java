/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.utils.validaciones;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;

/**
 *
 * @author Administrador
 */
public class DecimalNumbersInput implements KeyListener {

    JTextField field;

    public DecimalNumbersInput(JTextField field) {
        this.field = field;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        char c = ke.getKeyChar();

        if (c == '.' && field.getText().length() > 0) {
            //comprueba si el caracter insertado es el punto. 

            if (field.getText().contains(String.valueOf(c))) {
                //si ya se ha insertado un punto dentro del texto ignorar el caracter
                ke.consume();
            }
        } else if (!Character.isDigit(c)) {

            ke.consume();

            field.setToolTipText("Ingresa Solo Numeros");
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (field.getText().length() > 0) {
            double valueOf = Double.valueOf(field.getText());
        }
    }
}
