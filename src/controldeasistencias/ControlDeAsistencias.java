/**
 * ************************************************************************
 *
 * BioAsis - Control de Asistencia Biometrico Copyright(C) 2014 Eduardo Arvelaiz
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Created: 9/21/2014 File Name: ControlDeAsistencias.java File Desc: Main class 
 * Responsibilities: Init LogUtility, init TextUtility, invoke dependency che
 * cking, invoke preference loading, init the build of the program interface,
 * handle exiting JHTML back to system.
 *
 *************************************************************************
 */
package controldeasistencias;

import com.mysql.jdbc.CommunicationsException;
import controldeasistencias.Reportes.GenerarReportes;
import controldeasistencias.controllers.UsuarioJpaController;
import controldeasistencias.utils.splashscreen.SplashScreen;
import controldeasistencias.views.usuarios.LoginView;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.PersistenceException;
import javax.swing.JOptionPane;
import persistence.util.JpaUtil;
import controldeasistencias.utils.BDConfig;
import controldeasistencias.utils.Boot;
import controldeasistencias.utils.LogUtility;
import controldeasistencias.utils.PreferenceUtility;
/*
 import com.l2fprod.gui.plaf.skin.Skin;
 import com.l2fprod.gui.plaf.skin.SkinLookAndFeel;
 */

public class ControlDeAsistencias {

    private static SplashScreen splashScreen = null;
    private static PreferenceUtility startupPreferences = null;
    private static PreferenceUtility logPreferences = null;

    private static final String MAJOR_VERSION = "1";
    private static final String MINOR_VERSION = "0";
    private static final String PATCH_VERSION = "0";

    private static boolean loadPreferences(String baseDir) {
        splashScreen.update("Iniciando: Preferencias...");
        return PreferenceUtility.initPreferenceUtility(baseDir);
    }

    private static boolean initConnectToBD() throws CommunicationsException {
        splashScreen.update("Iniciando: Conexión con la base de datos....");
        UsuarioJpaController controller = new UsuarioJpaController( JpaUtil.getEntityManagerFactory(Boot.getProperties()));
        controller.getUsuarioCount();
        return true;
    }

    private static boolean initLogUtility() {
        splashScreen.update("Iniciando: Log...");

        boolean initStatus = LogUtility.initLogUtility();

        if (Boolean.valueOf(PreferenceUtility.getLogPreferences().getProperty("clearLogsOnStartup")).booleanValue() == true) {
            LogUtility.clearLogs();
        }

        return initStatus;
    }

    private static boolean initNetworkUtility() {
        splashScreen.update("Iniciando: Conexion de red...");
        return true;
    }

    public static boolean iniciarReportes() {
        splashScreen.update("Iniciando Reportes");
        GenerarReportes.preCompilar("ListaEmpleados");
        return true;
    }

    public static void main(String[] args) {
      
        Boot.getProperties();

        String baseDir = "";
        if (args.length > 0) {
            baseDir = args[0];
        }
        splashScreen = new SplashScreen(10);

        try {
            if (!(initLogUtility() && setUI() && initNetworkUtility() && initConnectToBD() && iniciarReportes())) {
                exitToSystem();
            }
        } catch (CommunicationsException | PersistenceException pe) {
            showErrorDeConexion(pe);
        }

        try {
            splashScreen.update("Iniciando programa...");
            Thread.sleep(1500);
        } catch (InterruptedException ex) {
            Logger.getLogger(ControlDeAsistencias.class.getName()).log(Level.SEVERE, null, ex);
        }
        splashScreen.dispose();
        LoginView loggin = new LoginView(null, true);
        loggin.setLocationRelativeTo(null);
        loggin.setVisible(true);
        loggin.toFront();
        /*        
         Main main = new Main();
         main.setExtendedState(JFrame.MAXIMIZED_BOTH);
         main.setVisible(true);
         */

    }


    public static void showErrorDeConexion(Exception ex) {
        int res = JOptionPane.showConfirmDialog(null, "No se ha podido Conectar con la base de datos", "Error", JOptionPane.YES_NO_OPTION);
        LogUtility.writeToErrorLog(ex.getMessage());
        Logger.getLogger(ControlDeAsistencias.class.getName()).log(Level.SEVERE, null, ex);

        if (res == JOptionPane.YES_OPTION) {
            //mostrar cuadro de configuracion de la base de datos
            BDConfig config = new BDConfig(null, true);
            config.setLocationRelativeTo(null);
            config.setVisible(true);
        } else {
            //salir del sistema
            System.exit(1);
        }
    }

    public static String getMajorVersion() {
        return MAJOR_VERSION;
    }

    public static String getMinorVersion() {
        return MINOR_VERSION;
    }

    public static String getPatchVersion() {
        return PATCH_VERSION;
    }

    public static void exitToSystem() {
        System.out.println("Guardando: configuración...");
        PreferenceUtility.storeAllPreferences();

        System.out.println("Cerrando aplicacion...");
        System.exit(0);
    }

    private static boolean setUI() {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return true;

    }
}
