/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.views.permisos.transaccion;

import controldeasistencias.Main;
import controldeasistencias.controllers.EmpleadoJpaController;
import controldeasistencias.controllers.PermisosEmpJpaController;
import controldeasistencias.controllers.PermisosJpaController;
import controldeasistencias.controllers.exceptions.NonexistentEntityException;
import controldeasistencias.entidades.Empleado;
import controldeasistencias.entidades.Permisos;
import controldeasistencias.entidades.PermisosEmp;
import controldeasistencias.utils.validaciones.OnlyTextInput;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import persistence.util.JpaUtil;
import utils.AccionSeleccionada;

/**
 *
 * @author Administrador
 */
public final class PermisosEmpleadoViews extends javax.swing.JPanel {

    private final PermisosEmpJpaController controller;
    private List<PermisosEmp> lista = null;
    private static PermisosEmp current;
    private static Empleado empleado;
    private static List<Empleado> listEmpleados;
    private final EmpleadoJpaController empleadosController;
    private static List<Permisos> permisos;
    private Permisos permisoSeleccionado; 

    private int index = 0;
    private final int countMaxResults = 1;
    private int countResults = 0;

    static AccionSeleccionada accion;

    private static final JDialog dialog = new JDialog(JOptionPane.getFrameForComponent(null),
            "BUSCAR EMPLEADO", true);

    /**
     * Creates new form PermisosEmp
     */
    public PermisosEmpleadoViews() {
        initComponents();
        controller = new PermisosEmpJpaController(JpaUtil.getEntityManagerFactory());
        empleadosController = new EmpleadoJpaController(JpaUtil.getEntityManagerFactory());
        listEmpleados = empleadosController.findEmpleadoEntities();

        cambiarEstadoBotones(AccionSeleccionada.NORMAL);
        actualizarContadorDeRegistros();
        txtNombre.addKeyListener(new OnlyTextInput(txtNombre));
        btnBuscar.setVisible(false);

        //si no es adminsitrador desabilitar los botones de nuevo, editar y eliminar
        if (Main.getNivelUsuario() != 0) {
            btnNuevo.setEnabled(false);
            btnEditar.setEnabled(false);
            btnEliminar.setEnabled(false);
        }

        cargarPermisoss();
        setCurrent();
    }

    private void cargarPermisoss() {
        PermisosJpaController controller = new PermisosJpaController(JpaUtil.getEntityManagerFactory());
        permisos = controller.findPermisosEntities();
        cargarListas();
    }

    public static void setCurrentDeBusqueda(PermisosEmp dep) {
        current = dep;
        actualizarCampos(current);
    }

    public static void setCurrentDeBusqueda(Empleado empl) {
        empleado = empl;
        txtNombre.setText(empl.getNombres() + ", " + empl.getApellido());

    }

    private static void actualizarCampos(PermisosEmp current) {
        diaPermiso.setDate(current.getFecha());
        Empleado emp = obtenerEmpleado(current.getIdEmpleado());
        txtNombre.setText(emp.getApellido() + ", " + emp.getNombres());
        Permisos permi = obtenerPermiso(current.getIdPermiso());
        txtDescripcion.setText(permi.getDescripcion());
        jcbPermisos.setSelectedItem(permi.getNombre());

    }

    public void actualizarContadorDeRegistros() {
        countResults = controller.getPermisosEmpCount();
    }

    public void setCurrent() {

        if (countResults <= 0) {
            return;
        }
        lista = controller.findPermisosEmpEntities(countMaxResults, index);
        current = lista.get(0);
        empleado = obtenerEmpleado(current.getIdEmpleado());

        if (null == current) {
            return;
        }

        actualizarCampos(current);
    }

    private void siguienteResultado() {
        if (index >= countResults - 1) {
            return;
        }
        index++;
        setCurrent();
    }

    private void anteriorResultado() {
        if (index <= 0) {
            return;
        }
        index--;
        setCurrent();
    }

    private void ultimoResultado() {
        index = countResults - 1;
        setCurrent();
    }

    private void primerResultado() {
        index = 0;
        setCurrent();
    }

    private static void cambiarEstadoBotones(AccionSeleccionada ac) {
        accion = ac;
        switch (accion) {
            case NORMAL:
                btnNuevo.setEnabled(true);
                btnEditar.setEnabled(true);
                btnGuardar.setEnabled(false);
                btnEliminar.setEnabled(true);
                btnBuscar.setEnabled(true);
                btnBuscar2.setEnabled(false);
                btnCancelar.setEnabled(false);
                btnPrimero.setEnabled(true);
                btnAnterior.setEnabled(true);
                btnSiguiente.setEnabled(true);
                btnUltimo.setEnabled(true);

                habilitarCampos(false);
                break;
            case NUEVO:
                limpiarCampos();
            case EDITANDO:
                btnNuevo.setEnabled(false);
                btnEditar.setEnabled(false);
                btnGuardar.setEnabled(true);
                btnEliminar.setEnabled(false);
                btnBuscar.setEnabled(false);
                btnBuscar2.setEnabled(true);
                btnCancelar.setEnabled(true);
                btnPrimero.setEnabled(false);
                btnAnterior.setEnabled(false);
                btnSiguiente.setEnabled(false);
                btnUltimo.setEnabled(false);
                habilitarCampos(true);
                break;
            case SELECCIOANDO:
                btnNuevo.setEnabled(false);
                btnEditar.setEnabled(false);
                btnGuardar.setEnabled(true);
                btnEliminar.setEnabled(false);
                btnBuscar.setEnabled(false);
                btnBuscar2.setEnabled(false);
                btnCancelar.setEnabled(true);
                btnPrimero.setEnabled(false);
                btnAnterior.setEnabled(false);
                btnSiguiente.setEnabled(false);
                btnUltimo.setEnabled(false);
                habilitarCampos(false);
        }
    }

    private static void habilitarCampos(Boolean enabled) {
        txtNombre.setEnabled(enabled);
        txtNombre.setEditable(enabled);
        txtNombre.requestFocus();
        txtDescripcion.setEnabled(enabled);
        txtDescripcion.setEditable(enabled);
        jcbPermisos.setEditable(enabled);

        jcbPermisos.setEnabled(enabled);

    }

    private static void limpiarCampos() {
        txtDescripcion.setText("");
        jcbPermisos.setSelectedIndex(0);

    }

    private boolean validarCampos() {
        return txtNombre.getText().length() >= 3;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        btnBuscar2 = new javax.swing.JButton();
        jcbPermisos = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        diaPermiso = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        btnVolver = new javax.swing.JButton();
        btnUltimo = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnPrimero = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("DATOS DEL DEPARTAMTO"));

        jLabel2.setText("EMPLEADO:");

        txtNombre.setEditable(false);
        txtNombre.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtNombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        txtDescripcion.setColumns(20);
        txtDescripcion.setRows(5);
        jScrollPane1.setViewportView(txtDescripcion);

        jLabel4.setText("DESCRIPCIÓN:");

        btnBuscar2.setText("...");
        btnBuscar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscar2ActionPerformed(evt);
            }
        });

        jcbPermisos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbPermisos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbPermisosItemStateChanged(evt);
            }
        });

        jLabel1.setText("PERMISO:");

        jLabel5.setText("DIA:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 408, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnBuscar2))
                        .addComponent(jcbPermisos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(diaPermiso, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jcbPermisos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(diaPermiso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addContainerGap())
        );

        btnVolver.setText("Volver");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        btnUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/StepForward16.gif"))); // NOI18N
        btnUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUltimoActionPerformed(evt);
            }
        });

        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Forward16.gif"))); // NOI18N
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        btnAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Back16.gif"))); // NOI18N
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        btnPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/StepBack16.gif"))); // NOI18N
        btnPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrimeroActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Stop16.gif"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Find16.gif"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Delete16.gif"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Save16.gif"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Edit16.gif"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/New16.gif"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnVolver)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPrimero)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAnterior)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSiguiente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUltimo)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnNuevo)
                        .addComponent(btnGuardar)
                        .addComponent(btnEditar)
                        .addComponent(btnEliminar)
                        .addComponent(btnBuscar)
                        .addComponent(btnCancelar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnPrimero)
                        .addComponent(btnAnterior)
                        .addComponent(btnSiguiente)
                        .addComponent(btnUltimo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVolver)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUltimoActionPerformed
        // TODO add your handling code here:
        ultimoResultado();
    }//GEN-LAST:event_btnUltimoActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        // TODO add your handling code here:
        siguienteResultado();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        // TODO add your handling code here:
        anteriorResultado();
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void btnPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrimeroActionPerformed
        // TODO add your handling code here:
        primerResultado();
    }//GEN-LAST:event_btnPrimeroActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        cancelarAccion();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        realizarBusquedaPermisos();
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        eliminarRegistro();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        guardarRegistro();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
        editarRegistro();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        nuevoRegistro();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnBuscar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscar2ActionPerformed
        // TODO add your handling code here:
        realizarBusquedaEmpleado();
    }//GEN-LAST:event_btnBuscar2ActionPerformed

    private void jcbPermisosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbPermisosItemStateChanged
        // TODO add your handling code here:
        if (jcbPermisos.getSelectedIndex() > -1) {
            txtDescripcion.setText(permisos.get(jcbPermisos.getSelectedIndex()).getDescripcion());
            permisoSeleccionado = permisos.get(jcbPermisos.getSelectedIndex());
        }
    }//GEN-LAST:event_jcbPermisosItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JButton btnAnterior;
    private static javax.swing.JButton btnBuscar;
    private static javax.swing.JButton btnBuscar2;
    private static javax.swing.JButton btnCancelar;
    private static javax.swing.JButton btnEditar;
    private static javax.swing.JButton btnEliminar;
    private static javax.swing.JButton btnGuardar;
    private static javax.swing.JButton btnNuevo;
    private static javax.swing.JButton btnPrimero;
    private static javax.swing.JButton btnSiguiente;
    private static javax.swing.JButton btnUltimo;
    private javax.swing.JButton btnVolver;
    private static com.toedter.calendar.JDateChooser diaPermiso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private static javax.swing.JComboBox jcbPermisos;
    private static javax.swing.JTextArea txtDescripcion;
    private static javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables

    private static void nuevoRegistro() {
        cambiarEstadoBotones(AccionSeleccionada.NUEVO);
    }

    private void editarRegistro() {
        if (current == null) {
            return;
        }
        cambiarEstadoBotones(AccionSeleccionada.EDITANDO);
    }

    private void cancelarAccion() {
        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro que desea cancelar?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            return;
        }
        cambiarEstadoBotones(AccionSeleccionada.NORMAL);
        setCurrent();
    }

    private void guardarRegistro() {
        if (!validarCampos()) {
            JOptionPane.showMessageDialog(this, "El nombre del departamento "
                    + "debe tener al menos 3", "Mensaje", JOptionPane.ERROR_MESSAGE);
            return;
        }

        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro guardar?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            cancelarAccion();
            return;
        }

        switch (accion) {
            case NUEVO:
                PermisosEmp nuevoPermisosEmp = new PermisosEmp();
                
                nuevoPermisosEmp.setFecha(diaPermiso.getDate());
                nuevoPermisosEmp.setIdEmpleado(empleado.getId());
                nuevoPermisosEmp.setIdPermiso(permisoSeleccionado.getId());
                
                controller.create(nuevoPermisosEmp);
                JOptionPane.showMessageDialog(this, "Se ha Guardado Correctamente.",
                        "Operacion éxitosa", JOptionPane.INFORMATION_MESSAGE);
                actualizarContadorDeRegistros();
                cambiarEstadoBotones(AccionSeleccionada.NORMAL);
                ultimoResultado();
                setCurrent();
                break;
            case EDITANDO:
                
                current.setFecha(diaPermiso.getDate());
                current.setIdEmpleado(empleado.getId());
                current.setIdPermiso(permisoSeleccionado.getId());
                try {
                    controller.edit(current);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(PermisosEmpleadoViews.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(PermisosEmpleadoViews.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(this, "Se ha Guardado Correctamente.",
                        "Operacion éxitosa", JOptionPane.INFORMATION_MESSAGE);
                cambiarEstadoBotones(AccionSeleccionada.NORMAL);
                setCurrent();
        }

    }

    private void eliminarRegistro() {
        if (current == null) {
            return;
        }
        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro de eliminar este registro?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            return;
        }

        try {
            controller.destroy(current.getId());
            JOptionPane.showMessageDialog(this, "Se ha borrado correctamente.",
                    "Operación éxitosa.", JOptionPane.INFORMATION_MESSAGE);
            actualizarContadorDeRegistros();
            anteriorResultado();
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(this, "No se puede borrar. ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private static void realizarBusquedaEmpleado() {
        TrabajadorBuscarViews buscarEmpleado = new TrabajadorBuscarViews(dialog);
        dialog.add(buscarEmpleado);
        dialog.setResizable(false);
        dialog.setSize(560, 380);
        dialog.setLocationRelativeTo(null);
        dialog.repaint();
        dialog.setVisible(true);

    }

    private static void realizarBusquedaPermisos() {
        PermisosBuscarViews buscarEmpleado = new PermisosBuscarViews(dialog);
        dialog.add(buscarEmpleado);
        dialog.setResizable(false);
        dialog.setSize(560, 380);
        dialog.setLocationRelativeTo(null);
        dialog.repaint();
        dialog.setVisible(true);

    }

    private void cargarListas() {
        cargarListas(jcbPermisos);

    }

    private void cargarListas(JComboBox jcb) {
        jcb.removeAllItems();
        if (permisos != null) {
            for (Permisos h : permisos) {
                jcb.addItem(h.getNombre());
            }
        }
    }

    private static Empleado obtenerEmpleado(Integer idEmpleado) {
        if (idEmpleado == null) {
            return null;
        }
        Empleado rEmpleado = null;
        for (Empleado e : listEmpleados) {
            if (e.getId() == idEmpleado) {
                rEmpleado = e;
                break;
            }
        }
        return rEmpleado;
    }

    private static Permisos obtenerPermiso(Integer idPermiso) {
        if (idPermiso == null) {
            return null;
        }
        Permisos rEmpleado = null;
        for (Permisos e : permisos) {
            if (e.getId() == idPermiso) {
                rEmpleado = e;
                break;
            }
        }
        return rEmpleado;
    }

}
