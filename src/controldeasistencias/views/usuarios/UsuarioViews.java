/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.views.usuarios;

import controldeasistencias.controllers.UsuarioJpaController;
import controldeasistencias.controllers.exceptions.NonexistentEntityException;
import controldeasistencias.entidades.Usuario;
import controldeasistencias.utils.validaciones.OnlyTextInput;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import persistence.util.JpaUtil;
import utils.AccionSeleccionada;
import controldeasistencias.Main;

/**
 *
 * @author Administrador
 */
public final class UsuarioViews extends javax.swing.JPanel {

    private final UsuarioJpaController controller;
    private List<Usuario> lista = null;
    private static Usuario current;

    private int index = 0;
    private final int countMaxResults = 1;
    private int countResults = 0;

    AccionSeleccionada accion;

    private final JDialog dialog = new JDialog(JOptionPane.getFrameForComponent(this), "BUSCAR USUARIO", true);

    /**
     * Creates new form Departamento
     */
    public UsuarioViews() {
        initComponents();
        controller = new UsuarioJpaController(JpaUtil.getEntityManagerFactory());

        cambiarEstadoBotones(AccionSeleccionada.NORMAL);
        actualizarContadorDeRegistros();
        txtNombres.addKeyListener(new OnlyTextInput(txtNombres));
        txtApellidos.addKeyListener(new OnlyTextInput(txtApellidos));
        setCurrent();

        //si no es adminsitrador desabilitar los botones de nuevo, editar y eliminar
        if (Main.getNivelUsuario() != 0) {
            this.btnNuevo.setEnabled(false);
            this.btnEditar.setEnabled(false);
            this.btnEliminar.setEnabled(false);
        }
    }

    public static void setCurrentDeBusqueda(Usuario usuario) {
        current = usuario;
        actualizarCampos(current);
    }

    private static void actualizarCampos(Usuario current) {
        txtNombres.setText(current.getNombres());
        txtApellidos.setText(current.getApellidos());
        txtClave.setText(current.getClave());
        txtUsuario.setText(current.getLogin());
        jcbTipo.setSelectedIndex(current.getTipo());
        jckActivo.setSelected(current.getEstatus());
    }

    public void actualizarContadorDeRegistros() {
        countResults = controller.getUsuarioCount();
    }

    public void setCurrent() {
        if (countResults <= 0) {
            return;
        }
        lista = controller.findUsuarioEntities(countMaxResults, index);
        current = lista.get(0);
        actualizarCampos(current);
    }

    private void siguienteResultado() {
        if (index >= countResults - 1) {
            return;
        }
        index++;
        setCurrent();
    }

    private void anteriorResultado() {
        if (index <= 0) {
            return;
        }
        index--;
        setCurrent();
    }

    private void ultimoResultado() {
        index = countResults - 1;
        setCurrent();
    }

    private void primerResultado() {
        index = 0;
        setCurrent();
    }

    private void cambiarEstadoBotones(AccionSeleccionada accion) {
        this.accion = accion;
        switch (accion) {
            case NORMAL:
                this.btnNuevo.setEnabled(true);
                this.btnEditar.setEnabled(true);
                this.btnGuardar.setEnabled(false);
                this.btnEliminar.setEnabled(true);
                this.btnBuscar.setEnabled(true);
                this.btnCancelar.setEnabled(false);
                this.btnPrimero.setEnabled(true);
                this.btnAnterior.setEnabled(true);
                this.btnSiguiente.setEnabled(true);
                this.btnUltimo.setEnabled(true);
                habilitarCampos(false);
                break;
            case NUEVO:
                limpiarCampos();
            case EDITANDO:
                this.btnNuevo.setEnabled(false);
                this.btnEditar.setEnabled(false);
                this.btnGuardar.setEnabled(true);
                this.btnEliminar.setEnabled(false);
                this.btnBuscar.setEnabled(false);
                this.btnCancelar.setEnabled(true);
                this.btnPrimero.setEnabled(false);
                this.btnAnterior.setEnabled(false);
                this.btnSiguiente.setEnabled(false);
                this.btnUltimo.setEnabled(false);
                habilitarCampos(true);
                break;
            case SELECCIOANDO:
                this.btnNuevo.setEnabled(false);
                this.btnEditar.setEnabled(false);
                this.btnGuardar.setEnabled(true);
                this.btnEliminar.setEnabled(false);
                this.btnBuscar.setEnabled(false);
                this.btnCancelar.setEnabled(true);
                this.btnPrimero.setEnabled(false);
                this.btnAnterior.setEnabled(false);
                this.btnSiguiente.setEnabled(false);
                this.btnUltimo.setEnabled(false);
                habilitarCampos(false);
        }
    }

    private void habilitarCampos(Boolean enabled) {
        txtNombres.setEnabled(enabled);
        txtNombres.setEditable(enabled);
        txtClave.setEditable(enabled);
        txtClave.setEnabled(enabled);
        txtUsuario.setEditable(enabled);
        txtUsuario.setEnabled(enabled);
        txtApellidos.setEditable(enabled);
        txtApellidos.setEnabled(enabled);
        jcbTipo.setEnabled(enabled);
        jckActivo.setEnabled(enabled);
    }

    private void limpiarCampos() {
        txtNombres.setText("");
        txtUsuario.setText("");
        txtClave.setText("");
        txtApellidos.setText("");
        jckActivo.setSelected(false);
        jcbTipo.setSelectedIndex(1);
    }

    private boolean validarCampos() {
        return txtNombres.getText().length() >= 3;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNombres = new javax.swing.JTextField();
        txtUsuario = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtClave = new javax.swing.JPasswordField();
        txtApellidos = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jcbTipo = new javax.swing.JComboBox();
        jckActivo = new javax.swing.JCheckBox();
        btnNuevo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnPrimero = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        btnUltimo = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnVolver = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("DATOS DEL USUARIO"));

        jLabel2.setText("NOMBRES:");

        txtNombres.setEditable(false);
        txtNombres.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtNombres.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        txtUsuario.setEditable(false);
        txtUsuario.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtUsuario.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel4.setText("USUARIO:");

        jLabel5.setText("CLAVE:");

        txtClave.setEditable(false);
        txtClave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));

        txtApellidos.setEditable(false);
        txtApellidos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtApellidos.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel6.setText("APELLIDOS:");

        jLabel7.setText("TIPO:");

        jcbTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ADMINISTRADOR", "NORMAL" }));

        jckActivo.setBackground(new java.awt.Color(255, 255, 255));
        jckActivo.setText("ACTIVO");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel7)
                        .addComponent(jLabel5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtClave)
                    .addComponent(txtApellidos, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombres, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jcbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jckActivo)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jcbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jckActivo)))
        );

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/New16.gif"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Edit16.gif"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Save16.gif"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/StepBack16.gif"))); // NOI18N
        btnPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrimeroActionPerformed(evt);
            }
        });

        btnAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Back16.gif"))); // NOI18N
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Forward16.gif"))); // NOI18N
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        btnUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/StepForward16.gif"))); // NOI18N
        btnUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUltimoActionPerformed(evt);
            }
        });

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Delete16.gif"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Find16.gif"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Stop16.gif"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnVolver.setText("Volver");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnVolver)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPrimero)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAnterior)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSiguiente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUltimo)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnNuevo)
                            .addComponent(btnGuardar)
                            .addComponent(btnEditar)
                            .addComponent(btnEliminar)
                            .addComponent(btnBuscar)
                            .addComponent(btnCancelar)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnPrimero)
                        .addComponent(btnAnterior)
                        .addComponent(btnSiguiente)
                        .addComponent(btnUltimo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVolver)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        nuevoRegistro();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
        editarRegistro();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        guardarRegistro();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrimeroActionPerformed
        // TODO add your handling code here:
        primerResultado();
    }//GEN-LAST:event_btnPrimeroActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        // TODO add your handling code here:
        anteriorResultado();
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        // TODO add your handling code here:
        siguienteResultado();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUltimoActionPerformed
        // TODO add your handling code here:
        ultimoResultado();
    }//GEN-LAST:event_btnUltimoActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        eliminarRegistro();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        realizarBusqueda();
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        cancelarAccion();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
    }//GEN-LAST:event_btnVolverActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnPrimero;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnUltimo;
    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private static javax.swing.JComboBox jcbTipo;
    private static javax.swing.JCheckBox jckActivo;
    private static javax.swing.JTextField txtApellidos;
    private static javax.swing.JPasswordField txtClave;
    private static javax.swing.JTextField txtNombres;
    private static javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables

    private void nuevoRegistro() {
        cambiarEstadoBotones(AccionSeleccionada.NUEVO);
    }

    private void editarRegistro() {
        cambiarEstadoBotones(AccionSeleccionada.EDITANDO);
    }

    private void cancelarAccion() {
        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro que desea cancelar?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            return;
        }
        cambiarEstadoBotones(AccionSeleccionada.NORMAL);
        setCurrent();
    }

    private void guardarRegistro() {
        if (!validarCampos()) {
            JOptionPane.showMessageDialog(this, "El nombre de usuario "
                    + "debe tener al menos 3", "Mensaje", JOptionPane.ERROR_MESSAGE);
            return;
        }

        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro guardar?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            cancelarAccion();
            return;
        }

        switch (accion) {
            case NUEVO:
                Usuario nuevoUsuario = new Usuario();
                nuevoUsuario.setNombres(txtNombres.getText().trim());
                nuevoUsuario.setApellidos(txtApellidos.getText().trim());
                nuevoUsuario.setEstatus(jckActivo.isSelected());
                nuevoUsuario.setLogin(txtUsuario.getText().trim());
                nuevoUsuario.setTipo(jcbTipo.getSelectedIndex());
                nuevoUsuario.setClave(txtClave.getText());
                controller.create(nuevoUsuario);
                JOptionPane.showMessageDialog(this, "Se ha Guardado Correctamente.",
                        "Operacion éxitosa", JOptionPane.INFORMATION_MESSAGE);
                actualizarContadorDeRegistros();
                cambiarEstadoBotones(AccionSeleccionada.NORMAL);
                ultimoResultado();
                setCurrent();
                break;
            case EDITANDO:
                current.setApellidos(txtApellidos.getText());
                current.setLogin(txtUsuario.getText().trim());
                current.setNombres(txtNombres.getText().trim());
                current.setClave(txtClave.getText().trim());
                current.setTipo(jcbTipo.getSelectedIndex());
                current.setEstatus(jckActivo.isSelected());
                try {
                    controller.edit(current);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(UsuarioViews.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(UsuarioViews.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(this, "Se ha Guardado Correctamente.",
                        "Operacion éxitosa", JOptionPane.INFORMATION_MESSAGE);
                cambiarEstadoBotones(AccionSeleccionada.NORMAL);
                setCurrent();
        }

    }

    private void eliminarRegistro() {
        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro de eliminar este registro?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            return;
        }

        try {
            controller.destroy(current);
            JOptionPane.showMessageDialog(this, "Se ha borrado correctamente.",
                    "Operación éxitosa.", JOptionPane.INFORMATION_MESSAGE);
            actualizarContadorDeRegistros();
            anteriorResultado();
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(this, "No se puede borrar. ", "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(UsuarioViews.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void realizarBusqueda() {
        UsuarioBuscarViews buscarTrabajador = new UsuarioBuscarViews(dialog);
        dialog.add(buscarTrabajador);
        dialog.setResizable(false);
        dialog.setSize(560, 380);
        dialog.setLocationRelativeTo(this);
        dialog.repaint();
        dialog.setVisible(true);
    }
}
