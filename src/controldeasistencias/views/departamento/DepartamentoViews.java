/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.views.departamento;

import controldeasistencias.Main;
import controldeasistencias.controllers.DepartamentoJpaController;
import controldeasistencias.controllers.exceptions.NonexistentEntityException;
import controldeasistencias.entidades.Departamento;
import controldeasistencias.utils.validaciones.OnlyTextInput;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import persistence.util.JpaUtil;
import utils.AccionSeleccionada;

/**
 *
 * @author Administrador
 */
public final class DepartamentoViews extends javax.swing.JPanel {

    private final DepartamentoJpaController controller;
    private List<Departamento> lista = null;
    private static Departamento current;

    private int index = 0;
    private final int countMaxResults = 1;
    private int countResults = 0;

    AccionSeleccionada accion;

    private final JDialog dialog = new JDialog(JOptionPane.getFrameForComponent(this), "BUSCAR DEPARTAMENTO", true);

    /**
     * Creates new form Departamento
     */
    public DepartamentoViews() {
        initComponents();
        controller = new DepartamentoJpaController(JpaUtil.getEntityManagerFactory());

        cambiarEstadoBotones(AccionSeleccionada.NORMAL);
        actualizarContadorDeRegistros();
        txtNombre.addKeyListener(new OnlyTextInput(txtNombre));
        setCurrent();
        
        //si no es adminsitrador desabilitar los botones de nuevo, editar y eliminar
        if (Main.getNivelUsuario() != 0) {
            this.btnNuevo.setEnabled(false);
            this.btnEditar.setEnabled(false);
            this.btnEliminar.setEnabled(false);
        }
    }

    public static void setCurrentDeBusqueda(Departamento dep) {
        current = dep;
        actualizarCampos(current);
    }

    private static void actualizarCampos(Departamento current) {
        txtDescripcion.setText(current.getDescripcion());
        txtNombre.setText(current.getNombre());
        jckActivo.setSelected(current.getActivo());
    }

    public void actualizarContadorDeRegistros() {
        countResults = controller.getDepartamentoCount();
    }

    public void setCurrent() {
        if (countResults <= 0) {
            return;
        }
        lista = controller.findDepartamentoEntities(countMaxResults, index);
        current = lista.get(0);
        actualizarCampos(current);
    }

    private void siguienteResultado() {
        if (index >= countResults - 1) {
            return;
        }
        index++;
        setCurrent();
    }

    private void anteriorResultado() {
        if (index <= 0) {
            return;
        }
        index--;
        setCurrent();
    }

    private void ultimoResultado() {
        index = countResults - 1;
        setCurrent();
    }

    private void primerResultado() {
        index = 0;
        setCurrent();
    }

    private void cambiarEstadoBotones(AccionSeleccionada accion) {
        this.accion = accion;
        switch (accion) {
            case NORMAL:
                this.btnNuevo.setEnabled(true);
                this.btnEditar.setEnabled(true);
                this.btnGuardar.setEnabled(false);
                this.btnEliminar.setEnabled(true);
                this.btnBuscar.setEnabled(true);
                this.btnCancelar.setEnabled(false);
                this.btnPrimero.setEnabled(true);
                this.btnAnterior.setEnabled(true);
                this.btnSiguiente.setEnabled(true);
                this.btnUltimo.setEnabled(true);
                habilitarCampos(false);
                break;
            case NUEVO:
                limpiarCampos();
            case EDITANDO:
                this.btnNuevo.setEnabled(false);
                this.btnEditar.setEnabled(false);
                this.btnGuardar.setEnabled(true);
                this.btnEliminar.setEnabled(false);
                this.btnBuscar.setEnabled(false);
                this.btnCancelar.setEnabled(true);
                this.btnPrimero.setEnabled(false);
                this.btnAnterior.setEnabled(false);
                this.btnSiguiente.setEnabled(false);
                this.btnUltimo.setEnabled(false);
                habilitarCampos(true);
                break;
            case SELECCIOANDO:
                this.btnNuevo.setEnabled(false);
                this.btnEditar.setEnabled(false);
                this.btnGuardar.setEnabled(true);
                this.btnEliminar.setEnabled(false);
                this.btnBuscar.setEnabled(false);
                this.btnCancelar.setEnabled(true);
                this.btnPrimero.setEnabled(false);
                this.btnAnterior.setEnabled(false);
                this.btnSiguiente.setEnabled(false);
                this.btnUltimo.setEnabled(false);
                habilitarCampos(false);
        }
    }

    private void habilitarCampos(Boolean enabled) {
        txtNombre.setEnabled(enabled);
        txtNombre.setEditable(enabled);
        txtNombre.requestFocus();
        jckActivo.setEnabled(enabled);
        txtDescripcion.setEnabled(enabled);
        txtDescripcion.setEditable(enabled);
    }

    private void limpiarCampos() {
        txtDescripcion.setText("");
        txtNombre.setText("");
        jckActivo.setSelected(false);
    }

    private boolean validarCampos() {
        return txtNombre.getText().length() >= 3;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jckActivo = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        btnVolver = new javax.swing.JButton();
        btnUltimo = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnPrimero = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("DATOS DEL DEPARTAMENTO"));

        jLabel2.setText("NOMBRE:");

        txtNombre.setEditable(false);
        txtNombre.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtNombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jckActivo.setBackground(new java.awt.Color(255, 255, 255));
        jckActivo.setText("ACTIVO");

        txtDescripcion.setColumns(20);
        txtDescripcion.setRows(5);
        jScrollPane1.setViewportView(txtDescripcion);

        jLabel4.setText("DESCRIPCIÓN:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 408, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addComponent(jckActivo)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jckActivo))
        );

        btnVolver.setText("Volver");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        btnUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/StepForward16.gif"))); // NOI18N
        btnUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUltimoActionPerformed(evt);
            }
        });

        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Forward16.gif"))); // NOI18N
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        btnAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Back16.gif"))); // NOI18N
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        btnPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/StepBack16.gif"))); // NOI18N
        btnPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrimeroActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Stop16.gif"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Find16.gif"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Delete16.gif"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Save16.gif"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Edit16.gif"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/New16.gif"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnVolver)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPrimero)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAnterior)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSiguiente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUltimo)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnNuevo)
                        .addComponent(btnGuardar)
                        .addComponent(btnEditar)
                        .addComponent(btnEliminar)
                        .addComponent(btnBuscar)
                        .addComponent(btnCancelar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnPrimero)
                        .addComponent(btnAnterior)
                        .addComponent(btnSiguiente)
                        .addComponent(btnUltimo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVolver)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUltimoActionPerformed
        // TODO add your handling code here:
        ultimoResultado();
    }//GEN-LAST:event_btnUltimoActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        // TODO add your handling code here:
        siguienteResultado();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        // TODO add your handling code here:
        anteriorResultado();
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void btnPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrimeroActionPerformed
        // TODO add your handling code here:
        primerResultado();
    }//GEN-LAST:event_btnPrimeroActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        cancelarAccion();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        realizarBusqueda();
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        eliminarRegistro();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        guardarRegistro();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
        editarRegistro();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        nuevoRegistro();
    }//GEN-LAST:event_btnNuevoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnPrimero;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnUltimo;
    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private static javax.swing.JCheckBox jckActivo;
    private static javax.swing.JTextArea txtDescripcion;
    private static javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables

    private void nuevoRegistro() {
        cambiarEstadoBotones(AccionSeleccionada.NUEVO);
    }

    private void editarRegistro() {
        if (current == null) {
            return;
        }
        cambiarEstadoBotones(AccionSeleccionada.EDITANDO);
    }

    private void cancelarAccion() {
        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro que desea cancelar?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            return;
        }
        cambiarEstadoBotones(AccionSeleccionada.NORMAL);
        setCurrent();
    }

    private void guardarRegistro() {
        if (!validarCampos()) {
            JOptionPane.showMessageDialog(this, "El nombre del departamento "
                    + "debe tener al menos 3", "Mensaje", JOptionPane.ERROR_MESSAGE);
            return;
        }

        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro guardar?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            cancelarAccion();
            return;
        }

        switch (accion) {
            case NUEVO:
                Departamento nuevoDepartamento = new Departamento();
                nuevoDepartamento.setNombre(txtNombre.getText().trim());
                nuevoDepartamento.setActivo(jckActivo.isSelected());
                nuevoDepartamento.setFecha(new Date());
                nuevoDepartamento.setFechaReg(new Date());
                //TODO: cambiar por la referencia de usuario
                nuevoDepartamento.setIdMUsuario(1);
                nuevoDepartamento.setDescripcion(txtDescripcion.getText());
                controller.create(nuevoDepartamento);
                JOptionPane.showMessageDialog(this, "Se ha Guardado Correctamente.",
                        "Operacion éxitosa", JOptionPane.INFORMATION_MESSAGE);
                actualizarContadorDeRegistros();
                cambiarEstadoBotones(AccionSeleccionada.NORMAL);
                ultimoResultado();
                setCurrent();
                break;
            case EDITANDO:
                current.setNombre(txtNombre.getText().trim());
                current.setDescripcion(txtDescripcion.getText());
                current.setActivo(jckActivo.isSelected());
                current.setFecha(new Date());
                try {
                    controller.edit(current);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(DepartamentoViews.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(DepartamentoViews.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(this, "Se ha Guardado Correctamente.",
                        "Operacion éxitosa", JOptionPane.INFORMATION_MESSAGE);
                cambiarEstadoBotones(AccionSeleccionada.NORMAL);
                setCurrent();
        }

    }

    private void eliminarRegistro() {
        if (current == null) {
            return;
        }
        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro de eliminar este registro?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            return;
        }

        try {
            controller.destroy(current);
            JOptionPane.showMessageDialog(this, "Se ha borrado correctamente.",
                    "Operación éxitosa.", JOptionPane.INFORMATION_MESSAGE);
            actualizarContadorDeRegistros();
            anteriorResultado();
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(this, "No se puede borrar. ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void realizarBusqueda() {
        DepartamentoBuscarViews buscarDepartamento = new DepartamentoBuscarViews(dialog);
        dialog.add(buscarDepartamento);
        dialog.setResizable(false);
        dialog.setSize(560, 380);
        dialog.setLocationRelativeTo(this);
        dialog.repaint();
        dialog.setVisible(true);
    }
}
