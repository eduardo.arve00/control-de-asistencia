/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.views.trabajador;

import com.griaule.fingerprintsdk.FormFinger;
import controldeasistencias.controllers.EmpleadoJpaController;
import controldeasistencias.controllers.exceptions.NonexistentEntityException;
import controldeasistencias.entidades.Cargo;
import controldeasistencias.entidades.Departamento;
import controldeasistencias.entidades.Empleado;
import controldeasistencias.utils.validaciones.FormatCedulaInput;
import controldeasistencias.utils.validaciones.OnlyTextInput;
import controldeasistencias.utils.validaciones.TelephoneFormatInput;
import controldeasistencias.Main;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import persistence.util.JpaUtil;
import utils.AccionSeleccionada;
import controldeasistencias.utils.LogUtility;

/**
 *
 * @author Administrador
 */
public final class TrabajadorViews extends javax.swing.JPanel {

    private final EmpleadoJpaController controller;
    private List<Empleado> lista = null;
    private static Empleado current;

    private static Departamento departamento;
    private static Cargo cargo;

    private int index = 0;
    private final int countMaxResults = 1;
    private int countResults = 0;

    AccionSeleccionada accion;

    private final JDialog dialog = new JDialog(JOptionPane.getFrameForComponent(this), "BUSCAR", true);

    /**
     * Creates new form Departamento
     */
    public TrabajadorViews() {
        initComponents();
        controller = new EmpleadoJpaController(JpaUtil.getEntityManagerFactory());
        initMyComponents();
        cambiarEstadoBotones(AccionSeleccionada.NORMAL);
        actualizarContadorDeRegistros();
        setCurrent();
        
         //si no es adminsitrador desabilitar los botones de nuevo, editar y eliminar
        if (Main.getNivelUsuario() != 0) {
            this.btnNuevo.setEnabled(false);
            this.btnEditar.setEnabled(false);
            this.btnEliminar.setEnabled(false);
        }
    }

    public static void setDepartamentDeBusqueda(Departamento dep) {
        departamento = dep;
        if (current != null) {
            current.setIdMDepartamentos(departamento);
            actualizarCampos(current);
        } else {
            txtDepartamento.setText(dep.getNombre());
        }
    }

    public static void setCargoDeBusqueda(Cargo car) {
        cargo = car;
        if (current != null) {
            current.setIdMCargo(cargo);
            actualizarCampos(current);
        } else {
            txtCargo.setText(car.getNombre());
        }
    }

    public static void setCurrentDeBusqueda(Empleado dep) {
        current = dep;
        actualizarCampos(current);
    }

    public void actualizarContadorDeRegistros() {
        countResults = controller.getEmpleadoCount();
    }

    public void setCurrent() {
        if (countResults <= 0) {
            return;
        }
        lista = controller.findEmpleadoEntities(countMaxResults, index);
        current = lista.get(0);
        actualizarCampos(current);
    }

    private static void actualizarCampos(Empleado current) {
        txtNombre.setText(current.getNombres());
        txtApellido.setText(current.getApellido());
        txaObservacion.setText(current.getObservacion());
        txtCedula.setText(current.getCedula());
        jcdFechaNac.setDate(current.getFechaNac());
        txtDepartamento.setText(current.getIdMDepartamentos().getNombre());
        txtCargo.setText(current.getIdMCargo().getNombre());
        jcdFechaIngreso.setDate(current.getFechaIngreso());
        jcbEstadoCivil.setSelectedIndex(current.getEdoCivil());
        jcbNacionalidad.setSelectedIndex(current.getNacionalidad());
        txtTelefono.setText(current.getTelefono());
        txaDireccion.setText(current.getDirHab());
        txtDescModalidad.setText(current.getModalidadDes());
        jcbModalidad.setSelectedItem(current.getModalidad());
    }

    private void siguienteResultado() {
        if (index >= countResults - 1) {
            return;
        }
        index++;
        setCurrent();
    }

    private void anteriorResultado() {
        if (index <= 0) {
            return;
        }
        index--;
        setCurrent();
    }

    private void ultimoResultado() {
        index = countResults - 1;
        setCurrent();
    }

    private void primerResultado() {
        index = 0;
        setCurrent();
    }

    private void cambiarEstadoBotones(AccionSeleccionada accion) {
        this.accion = accion;
        switch (accion) {
            case NORMAL:
                this.btnNuevo.setEnabled(true);
                this.btnEditar.setEnabled(true);
                this.btnGuardar.setEnabled(false);
                this.btnEliminar.setEnabled(true);
                this.btnBuscar.setEnabled(true);
                this.btnCancelar.setEnabled(false);
                this.btnPrimero.setEnabled(true);
                this.btnAnterior.setEnabled(true);
                this.btnSiguiente.setEnabled(true);
                this.btnUltimo.setEnabled(true);
                habilitarCampos(false);
                break;
            case NUEVO:
                limpiarCampos();
            case EDITANDO:
                this.btnNuevo.setEnabled(false);
                this.btnEditar.setEnabled(false);
                this.btnGuardar.setEnabled(true);
                this.btnEliminar.setEnabled(false);
                this.btnBuscar.setEnabled(false);
                this.btnCancelar.setEnabled(true);
                this.btnPrimero.setEnabled(false);
                this.btnAnterior.setEnabled(false);
                this.btnSiguiente.setEnabled(false);
                this.btnUltimo.setEnabled(false);
                habilitarCampos(true);
                break;
            case SELECCIOANDO:
                this.btnNuevo.setEnabled(false);
                this.btnEditar.setEnabled(false);
                this.btnGuardar.setEnabled(true);
                this.btnEliminar.setEnabled(false);
                this.btnBuscar.setEnabled(false);
                this.btnCancelar.setEnabled(true);
                this.btnPrimero.setEnabled(false);
                this.btnAnterior.setEnabled(false);
                this.btnSiguiente.setEnabled(false);
                this.btnUltimo.setEnabled(false);
                habilitarCampos(false);

        }
        habilitarBotonHuella();
    }

    private void habilitarCampos(Boolean enabled) {
        txtNombre.setEnabled(enabled);
        txtNombre.setEditable(enabled);
        txtApellido.setEditable(enabled);
        txtApellido.setEnabled(enabled);
        txtCedula.setEditable(enabled);
        txtCedula.setEnabled(enabled);
        jcdFechaNac.setEnabled(enabled);
        btnBuscarCargo.setEnabled(enabled);
        btnDepartamento.setEnabled(enabled);
        jcdFechaIngreso.setEnabled(enabled);
        jcbEstadoCivil.setEnabled(enabled);
        jcbNacionalidad.setEnabled(enabled);
        txtTelefono.setEnabled(enabled);
        txaDireccion.setEditable(enabled);
        txaObservacion.setEditable(enabled);
        jcbModalidad.setEnabled(enabled);
        txtDescModalidad.setEnabled(enabled);
    }

    private void limpiarCampos() {
        txtNombre.setText("");
        txtCedula.setText("");
        txtApellido.setText("");
        jcdFechaNac.setDate(new Date());
        txtDepartamento.setText("");
        txtCargo.setText("");
        jcdFechaIngreso.setDate(new Date());
        jcbEstadoCivil.setSelectedIndex(0);
        jcbNacionalidad.setSelectedIndex(0);
        txtTelefono.setText("");
        txaDireccion.setText("");
        txaObservacion.setText("");
    }

    private void habilitarBotonHuella() {
        if (this.accion == AccionSeleccionada.EDITANDO) {
            this.btnHuella.setEnabled(true);
        } else {
            this.btnHuella.setEnabled(false);
        }
    }


    private boolean validarCampos() {
        
        if (jcbModalidad.getSelectedIndex() == 0 ) {
            JOptionPane.showMessageDialog(this, "Seleccione una modalidad.",
                    "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            jcbModalidad.requestFocus();
            return false; 
        }
            
        return txtNombre.getText().length() >= 3;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtCedula = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtApellido = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtCargo = new javax.swing.JTextField();
        btnBuscarCargo = new javax.swing.JButton();
        btnDepartamento = new javax.swing.JButton();
        txtDepartamento = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jcdFechaIngreso = new com.toedter.calendar.JDateChooser();
        jLabel9 = new javax.swing.JLabel();
        jcbEstadoCivil = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jcbNacionalidad = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        jcdFechaNac = new com.toedter.calendar.JDateChooser();
        jLabel13 = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txaDireccion = new javax.swing.JTextArea();
        btnHuella = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txaObservacion = new javax.swing.JTextArea();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jcbModalidad = new javax.swing.JComboBox();
        txtDescModalidad = new javax.swing.JTextField();
        btnVolver = new javax.swing.JButton();
        btnUltimo = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnPrimero = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();

        jDateChooser2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jDateChooser2.setMinimumSize(new java.awt.Dimension(33, 22));
        jDateChooser2.setPreferredSize(new java.awt.Dimension(93, 22));

        jLabel11.setText("FECHA IN:");

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("DATOS DEL TRABAJADOR"));

        jLabel2.setText("NOMBRE:");

        txtNombre.setEditable(false);
        txtNombre.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtNombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        txtCedula.setEditable(false);
        txtCedula.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtCedula.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel4.setText("CEDULA:");

        jLabel5.setText("APELLIDO:");

        txtApellido.setEditable(false);
        txtApellido.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtApellido.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel6.setText("CARGO:");

        txtCargo.setEditable(false);
        txtCargo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtCargo.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        btnBuscarCargo.setText("...");
        btnBuscarCargo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCargoActionPerformed(evt);
            }
        });

        btnDepartamento.setText("...");
        btnDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDepartamentoActionPerformed(evt);
            }
        });

        txtDepartamento.setEditable(false);
        txtDepartamento.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtDepartamento.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel7.setText("DEPARTAMENTO:");

        jLabel8.setText("FECHA DE INGRESO:");

        jcdFechaIngreso.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jcdFechaIngreso.setMinimumSize(new java.awt.Dimension(33, 26));
        jcdFechaIngreso.setPreferredSize(new java.awt.Dimension(93, 26));

        jLabel9.setText("EDO. CIVIL:");

        jcbEstadoCivil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SOLTERO", "CASADO" }));

        jLabel10.setText("NACIONALIDAD:");

        jcbNacionalidad.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "VENEZOLANO(A)", "EXTRANJERO(A)" }));

        jLabel12.setText("FECHA DE NAC.:");

        jcdFechaNac.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jcdFechaNac.setMinimumSize(new java.awt.Dimension(33, 26));
        jcdFechaNac.setPreferredSize(new java.awt.Dimension(93, 22));

        jLabel13.setText("TELÉFONO:");

        txtTelefono.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtTelefono.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtTelefono.setEnabled(false);

        jLabel14.setText("DIRECCION:");

        txaDireccion.setColumns(20);
        txaDireccion.setRows(5);
        jScrollPane1.setViewportView(txaDireccion);

        btnHuella.setText("HUELLA");
        btnHuella.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHuellaActionPerformed(evt);
            }
        });

        txaObservacion.setColumns(20);
        txaObservacion.setRows(5);
        jScrollPane2.setViewportView(txaObservacion);

        jLabel15.setText("OBSERVACIÓN:");

        jLabel16.setText("MODALIDAD:");

        jLabel17.setText("DESC. MODALIDAD:");

        jcbModalidad.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONAR", "FIJO", "CONTRATADO", "SUPLENTE" }));

        txtDescModalidad.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        txtDescModalidad.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(33, 33, 33)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.TRAILING))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(txtDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(btnDepartamento))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(txtCargo, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(btnBuscarCargo))
                                        .addComponent(jcbEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jcbModalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtDescModalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jcdFechaIngreso, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jcbNacionalidad, javax.swing.GroupLayout.Alignment.LEADING, 0, 133, Short.MAX_VALUE))
                                        .addComponent(jcdFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel13)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(18, 18, Short.MAX_VALUE)
                            .addComponent(btnHuella))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(jLabel14)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(188, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jcdFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarCargo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDepartamento))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jcbModalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtDescModalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jcdFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jcbEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jcbNacionalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnHuella))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnVolver.setText("Volver");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        btnUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/StepForward16.gif"))); // NOI18N
        btnUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUltimoActionPerformed(evt);
            }
        });

        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Forward16.gif"))); // NOI18N
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        btnAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Back16.gif"))); // NOI18N
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        btnPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/StepBack16.gif"))); // NOI18N
        btnPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrimeroActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Stop16.gif"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Find16.gif"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Delete16.gif"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Save16.gif"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/Edit16.gif"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siap/imagenes/New16.gif"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnVolver)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                        .addComponent(btnPrimero)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAnterior)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSiguiente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUltimo))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnNuevo)
                        .addComponent(btnGuardar)
                        .addComponent(btnEditar)
                        .addComponent(btnEliminar)
                        .addComponent(btnBuscar)
                        .addComponent(btnCancelar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnPrimero)
                        .addComponent(btnAnterior)
                        .addComponent(btnSiguiente)
                        .addComponent(btnUltimo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVolver)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnHuellaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHuellaActionPerformed
        // TODO add your handling code here:
        new FormFinger(current.getId()).setVisible(true);
    }//GEN-LAST:event_btnHuellaActionPerformed

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnBuscarCargoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCargoActionPerformed
        // TODO add your handling code here:
        realizarBusquedaCargo();
    }//GEN-LAST:event_btnBuscarCargoActionPerformed

    private void btnDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDepartamentoActionPerformed
        // TODO add your handling code here:
        realizarBusquedaDepartamento();
    }//GEN-LAST:event_btnDepartamentoActionPerformed

    private void btnUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUltimoActionPerformed
        // TODO add your handling code here:
        ultimoResultado();
    }//GEN-LAST:event_btnUltimoActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        // TODO add your handling code here:
        siguienteResultado();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        // TODO add your handling code here:
        anteriorResultado();
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void btnPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrimeroActionPerformed
        // TODO add your handling code here:
        primerResultado();
    }//GEN-LAST:event_btnPrimeroActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        cancelarAccion();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        realizarBusqueda();
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        eliminarRegistro();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        guardarRegistro();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
        editarRegistro();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        nuevoRegistro();
    }//GEN-LAST:event_btnNuevoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnBuscarCargo;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnDepartamento;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnHuella;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnPrimero;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnUltimo;
    private javax.swing.JButton btnVolver;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private static javax.swing.JComboBox jcbEstadoCivil;
    private static javax.swing.JComboBox jcbModalidad;
    private static javax.swing.JComboBox jcbNacionalidad;
    private static com.toedter.calendar.JDateChooser jcdFechaIngreso;
    private static com.toedter.calendar.JDateChooser jcdFechaNac;
    private static javax.swing.JTextArea txaDireccion;
    private static javax.swing.JTextArea txaObservacion;
    private static javax.swing.JTextField txtApellido;
    private static javax.swing.JTextField txtCargo;
    private static javax.swing.JTextField txtCedula;
    private static javax.swing.JTextField txtDepartamento;
    private static javax.swing.JTextField txtDescModalidad;
    private static javax.swing.JTextField txtNombre;
    private static javax.swing.JTextField txtTelefono;
    // End of variables declaration//GEN-END:variables

    private void nuevoRegistro() {
        current = null;
        cambiarEstadoBotones(AccionSeleccionada.NUEVO);
    }

    private void editarRegistro() {
        cambiarEstadoBotones(AccionSeleccionada.EDITANDO);
    }

    private void cancelarAccion() {
        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro que desea cancelar?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            return;
        }
        cambiarEstadoBotones(AccionSeleccionada.NORMAL);
        setCurrent();
    }

    private void guardarRegistro() {
        if (!validarCampos()) {
           return;
        }

        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro guardar?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            cancelarAccion();
            return;
        }

        switch (accion) {
            case NUEVO:

                if (controller.existEmpleado(txtCedula.getText().trim())) {
                    JOptionPane.showMessageDialog(this, "Ya existe un empleado "
                            + "con el numero de cédula insertado.", "Mensaje",
                            JOptionPane.ERROR_MESSAGE);

                    return;
                }

                Empleado nuevoEmpleado = new Empleado();
                nuevoEmpleado.setNombres(txtNombre.getText().trim());
                nuevoEmpleado.setApellido(txtApellido.getText().trim());
                nuevoEmpleado.setCedula(txtCedula.getText().trim());
                nuevoEmpleado.setFechaNac(jcdFechaNac.getDate());
                //TODO: Sustituir por el id del objeto
                nuevoEmpleado.setModalidad((String) jcbModalidad.getSelectedItem());
                nuevoEmpleado.setModalidadDes(txtDescModalidad.getText());
                nuevoEmpleado.setIdMCargo(cargo);
                nuevoEmpleado.setIdMDepartamentos(departamento);
                nuevoEmpleado.setFechaIngreso(jcdFechaIngreso.getDate());
                nuevoEmpleado.setEdoCivil(jcbEstadoCivil.getSelectedIndex());
                nuevoEmpleado.setNacionalidad(jcbNacionalidad.getSelectedIndex());
                nuevoEmpleado.setTelefono(txtTelefono.getText().trim());
                nuevoEmpleado.setDirHab(txaDireccion.getText().trim());
                nuevoEmpleado.setObservacion(txaObservacion.getText());
                //TODO: cambiar por el id del usuario que tiene la session
                nuevoEmpleado.setIdMUsuario(1);
                nuevoEmpleado.setFechaReg(new Date());
                try {
                    controller.create(nuevoEmpleado);
                    JOptionPane.showMessageDialog(this, "Se ha Guardado Correctamente.",
                            "Operacion éxitosa", JOptionPane.INFORMATION_MESSAGE);

                } catch (Exception ex) {
                    LogUtility.writeToErrorLog(ex.getMessage());
                    JOptionPane.showMessageDialog(this, "Seleccione el cargo"
                            + " y departameto..",
                            "Error", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                actualizarContadorDeRegistros();
                cambiarEstadoBotones(AccionSeleccionada.NORMAL);
                ultimoResultado();
                setCurrent();
                break;
            case EDITANDO:
                current.setNombres(txtNombre.getText().trim());
                current.setApellido(txtApellido.getText().trim());
                current.setCedula(txtCedula.getText().trim());
                current.setFechaNac(jcdFechaNac.getDate());
                //TODO: Sustituir por el id del objeto

                //se actualiza directamente despues de la llamada al metodo
                //current.setIdMCargo(null);
                //current.setIdMDepartamentos(1);
                current.setModalidad((String) jcbModalidad.getSelectedItem());
                current.setModalidadDes(txtDescModalidad.getText());
                current.setFechaIngreso(jcdFechaIngreso.getDate());
                current.setEdoCivil(jcbEstadoCivil.getSelectedIndex());
                current.setNacionalidad(jcbNacionalidad.getSelectedIndex());
                current.setTelefono(txtTelefono.getText().trim());
                current.setDirHab(txaDireccion.getText().trim());
                current.setObservacion(txaObservacion.getText());
                //TODO: cambiar por el id del usuario que tiene la session
                current.setIdMUsuario(1);
                current.setFecha(new Date());
                try {
                    controller.edit(current);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(TrabajadorViews.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(TrabajadorViews.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(this, "Se ha Guardado Correctamente.",
                        "Operacion éxitosa", JOptionPane.INFORMATION_MESSAGE);
                cambiarEstadoBotones(AccionSeleccionada.NORMAL);
                setCurrent();
        }

    }

    private void eliminarRegistro() {
        int resultUserSelected = JOptionPane.showConfirmDialog(this,
                "¿Esta seguro de eliminar este registro?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (resultUserSelected == JOptionPane.NO_OPTION) {
            return;
        }

        try {
            controller.destroy(current);
            JOptionPane.showMessageDialog(this, "Se ha borrado correctamente.",
                    "Operación éxitosa.", JOptionPane.INFORMATION_MESSAGE);
            actualizarContadorDeRegistros();
            anteriorResultado();
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(this, "No se puede borrar. ", "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(TrabajadorViews.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void mostrarDialogoDeBusqueda(JPanel panel) {
        dialog.add(panel, 0);
        dialog.setResizable(false);
        dialog.setSize(560, 380);
        dialog.setLocationRelativeTo(this);

        try {
            dialog.remove(1);
        } catch (Exception e) {
        }

        dialog.repaint();
        dialog.setVisible(true);
    }

    private void realizarBusqueda() {
        TrabajadorBuscarViews buscarTrabajador = new TrabajadorBuscarViews(dialog);
        mostrarDialogoDeBusqueda(buscarTrabajador);
    }

    private void realizarBusquedaDepartamento() {
        DepartamentoBuscarViews buscarDepartamento = new DepartamentoBuscarViews(dialog);
        mostrarDialogoDeBusqueda(buscarDepartamento);
    }

    private void realizarBusquedaCargo() {
        CargosBuscarViews buscarCargo = new CargosBuscarViews(dialog);
        mostrarDialogoDeBusqueda(buscarCargo);
    }

    private void initMyComponents() {
        txtApellido.addKeyListener(new OnlyTextInput(txtApellido));
        txtNombre.addKeyListener(new OnlyTextInput(txtNombre));
        txtTelefono.addKeyListener(new TelephoneFormatInput(txtTelefono));
        txtCedula.addKeyListener(new FormatCedulaInput(txtCedula));

    }
}
