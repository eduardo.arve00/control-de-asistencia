package controldeasistencias.views.asistencia;

import controldeasistencias.entidades.Asistencia;
import controldeasistencias.entidades.Empleado;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import persistence.util.JpaUtil;

/**
 *
 * @author Administrador
 */
public class InasistenciasViews extends javax.swing.JPanel {

    private int maxResult = 10;
    private int currentPage = 0;

    private long timeLastSelected = 0;

    // -------------------- OBCIONES DE BUSQUEDA -----------------------------//
    //------------------------------------------------------------------------//
    private String[][] modeloBusqueda = {
        /*
         1. Nombre: nombre del campo de busqueda
         2. Consulta: nombre de la consulta con JPA
         3. Nombre del campo clave para el parametro
         4. Tipo de dato del parametro
         */
        {"CEDULA", "cedula", "cedula", "string"},
        {"NOMBRE", "nombres", "nombreCompleto", "string"}
    };

    DefaultTableModel model;
    List<Empleado> lista = new ArrayList();
    private final EntityManager entityManager;
    private JDialog dialog;

    public InasistenciasViews() {
        entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        initComponents();

        String[] columnNames = {"CEDULA", "EMPLEADO", "OBSERVACION"};

        model = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        model.setColumnIdentifiers(columnNames);

        tblResultados.setModel(model);
        setParametrosBusqueda();
        cargarDatosBD();
        jdcFecha.setDate(new Date());
    }

    private void setParametrosBusqueda() {
        cbOpcionesDeBusqueda.removeAllItems();
        for (int i = 0; i < modeloBusqueda.length; i++) {
            String[] strings = modeloBusqueda[i];
            String nombre = strings[0];
            cbOpcionesDeBusqueda.addItem(nombre);
        }
    }

    private void realizarBusqueda() {
        if (txtBuscar.getText().trim().isEmpty()) {
            return;
        }

        int idx = this.cbOpcionesDeBusqueda.getSelectedIndex();
        String valores[] = modeloBusqueda[idx];

        String valor = this.txtBuscar.getText().trim();

        Date selectedDate = jdcFecha.getDate();
        selectedDate.setHours(0);
        selectedDate.setMinutes(0);
        selectedDate.setSeconds(0);

        String strQuery = "select * from empleado where cedula not in (select cedula "
                + "from asistencia where fecha =?1) ";

        strQuery = strQuery + " and empleado.`"+valores[1]+"` LIKE ?2";

        Query query = entityManager.createNativeQuery(strQuery, Empleado.class)
                .setParameter(1, selectedDate);
        System.out.println(query.toString());
        switch (valores[3]) {
            case "int":
                try {
                    query.setParameter(2, Integer.parseInt(valor));
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(this,
                            "El ID debe ser un número.", "Mensaje",
                            JOptionPane.INFORMATION_MESSAGE);
                    txtBuscar.setText("");
                    txtBuscar.requestFocus();
                }
                break;
            case "double":
                query.setParameter(2, Double.parseDouble(valor));
                break;
            case "string":
                query.setParameter(2, "%" + valor + "%");

                break;
        }

        try {
            lista = query.getResultList();
            cargarTabla(lista);
        } catch (PersistenceException pe) {

        }
    }

    private void realizarBusquedaFecha() {
        //JpaUtil.getEntityManagerFactory().getCache().evict(Asistencia.class);

        Query query = entityManager.createNamedQuery("Asistencia.findByFecha");
        query.setParameter("fecha", jdcFecha.getDate());

        try {
            lista = query.getResultList();
            cargarTabla(lista);

        } catch (PersistenceException pe) {
            pe.printStackTrace();
        }
    }

    private void cargarDatosBD() {

        if (jdcFecha.getDate() == null) {
            return;
        }

        JpaUtil.getEntityManagerFactory().getCache().evict(Asistencia.class);

        Date selectedDate = jdcFecha.getDate();
        selectedDate.setHours(0);
        selectedDate.setMinutes(0);
        selectedDate.setSeconds(0);

        String query = "select * from empleado where cedula not in (select cedula "
                + "from asistencia where fecha =?1);";
        Query myQuery = entityManager.createNativeQuery(query, Empleado.class)
                .setParameter(1, selectedDate);

        lista = myQuery.getResultList();
        //System.out.println("Page:" + currentPage + " Max: " + maxResult + " Begin: " + maxResult * currentPage);
        if (lista.size() > 0) {
            currentPage--;
            lista = myQuery.getResultList();
            cargarTabla(lista);
        } else {
            limpiarTabla();
        }
    }

    void cargarTabla(List list) {
        System.out.println("Cargando resultados");
        limpiarTabla();
        Empleado empleado;
        for (Object object : list) {
            empleado = (Empleado) object;
            model.addRow(new Object[]{empleado.getCedula(), empleado.getNombres() + ", " + empleado.getApellido(), "INAISTENTE"});
        }
    }

    void limpiarTabla() {
        System.out.println("Limpiando tabla");
        while (model.getRowCount() > 0) {
            model.removeRow(0);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblResultados = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        cbOpcionesDeBusqueda = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txtBuscar = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jdcFecha = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        cbCantidadResultados = new javax.swing.JComboBox();
        btnPgAnt = new javax.swing.JButton();
        btnPgProx = new javax.swing.JButton();
        btnPagActualizar = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        tblResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblResultadosMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tblResultadosMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblResultadosMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblResultadosMouseReleased(evt);
            }
        });
        tblResultados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblResultadosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblResultados);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setText("Opciones de Busqueda");

        jLabel5.setText("Dato a Buscar");

        jButton1.setText("Buscar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Restablecer");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jdcFecha.setMaxSelectableDate(new Date());
        jdcFecha.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jdcFechaMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jdcFechaMouseReleased(evt);
            }
        });
        jdcFecha.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jdcFechaFocusLost(evt);
            }
        });
        jdcFecha.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdcFechaPropertyChange(evt);
            }
        });

        jLabel1.setText("FECHA:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(cbOpcionesDeBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel5)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdcFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbOpcionesDeBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton1)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2)
                        .addComponent(jLabel1))
                    .addComponent(jdcFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );

        jButton4.setText("Salir");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        cbCantidadResultados.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "10", "20", "50", "100" }));

        btnPgAnt.setText("<");
        btnPgAnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPgAntActionPerformed(evt);
            }
        });

        btnPgProx.setText(">");
        btnPgProx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPgProxActionPerformed(evt);
            }
        });

        btnPagActualizar.setText("Actualizar");
        btnPagActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagActualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 734, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(cbCantidadResultados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPgAnt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPgProx)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPagActualizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbCantidadResultados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnPgAnt)
                        .addComponent(btnPgProx)
                        .addComponent(btnPagActualizar))
                    .addComponent(jButton4))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tblResultadosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblResultadosKeyPressed
        // TODO add your handling code here:
        seleccionarItem();
    }//GEN-LAST:event_tblResultadosKeyPressed

    private void tblResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMouseClicked
        // TODO add your handling code here:
        seleccionarItem();
    }//GEN-LAST:event_tblResultadosMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        realizarBusqueda();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        cargarDatosBD();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void tblResultadosMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tblResultadosMouseExited

    private void tblResultadosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMousePressed
        // TODO add your handling code here:

    }//GEN-LAST:event_tblResultadosMousePressed

    private void tblResultadosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tblResultadosMouseReleased

    private void btnPgAntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPgAntActionPerformed
        // TODO add your handling code here:
        if (currentPage != 0) {
            currentPage--;
            cargarDatosBD();
        }
    }//GEN-LAST:event_btnPgAntActionPerformed

    private void btnPgProxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPgProxActionPerformed
        // TODO add your handling code here:
        if (lista.size() >= maxResult) {
            currentPage++;
            cargarDatosBD();
        }
    }//GEN-LAST:event_btnPgProxActionPerformed

    private void btnPagActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagActualizarActionPerformed
        // TODO add your handling code here:
        cargarDatosBD();
    }//GEN-LAST:event_btnPagActualizarActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);

    }//GEN-LAST:event_jButton4ActionPerformed

    private void jdcFechaPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdcFechaPropertyChange
        // TODO add your handling code here:
        cargarDatosBD();
    }//GEN-LAST:event_jdcFechaPropertyChange

    private void jdcFechaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jdcFechaMouseReleased
        // TODO add your handling code here:
        cargarDatosBD();
    }//GEN-LAST:event_jdcFechaMouseReleased

    private void jdcFechaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jdcFechaMouseClicked
        // TODO add your handling code here:
        cargarDatosBD();
    }//GEN-LAST:event_jdcFechaMouseClicked

    private void jdcFechaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jdcFechaFocusLost
        // TODO add your handling code here:
        cargarDatosBD();
    }//GEN-LAST:event_jdcFechaFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPagActualizar;
    private javax.swing.JButton btnPgAnt;
    private javax.swing.JButton btnPgProx;
    private javax.swing.JComboBox cbCantidadResultados;
    private javax.swing.JComboBox cbOpcionesDeBusqueda;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private com.toedter.calendar.JDateChooser jdcFecha;
    private javax.swing.JTable tblResultados;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    private void seleccionarItem() {
        Empleado cargo = obtenerItemSeleccionado();
    }

    private Empleado obtenerItemSeleccionado() {
        if (lista.size() <= 0) {
            return null;
        }
        return lista.get(this.tblResultados.getSelectedRow() > -1 ? this.tblResultados.getSelectedRow() : 0);
    }
}
