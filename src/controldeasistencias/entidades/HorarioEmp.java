/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "horario_emp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HorarioEmp.findAll", query = "SELECT h FROM HorarioEmp h"),
    @NamedQuery(name = "HorarioEmp.findById", query = "SELECT h FROM HorarioEmp h WHERE h.id = :id"),
    @NamedQuery(name = "HorarioEmp.findByDescripcion", query = "SELECT h FROM HorarioEmp h WHERE h.descripcion = :descripcion"),
    @NamedQuery(name = "HorarioEmp.findByActivo", query = "SELECT h FROM HorarioEmp h WHERE h.activo = :activo"),
    @NamedQuery(name = "HorarioEmp.findByEliminado", query = "SELECT h FROM HorarioEmp h WHERE h.eliminado = :eliminado"),
    @NamedQuery(name = "HorarioEmp.findByFecha", query = "SELECT h FROM HorarioEmp h WHERE h.fecha = :fecha"),
    @NamedQuery(name = "HorarioEmp.findByFechaReg", query = "SELECT h FROM HorarioEmp h WHERE h.fechaReg = :fechaReg"),
    @NamedQuery(name = "HorarioEmp.findByFechaVenc", query = "SELECT h FROM HorarioEmp h WHERE h.fechaVenc = :fechaVenc"),
    @NamedQuery(name = "HorarioEmp.findByIdMUsuario", query = "SELECT h FROM HorarioEmp h WHERE h.idMUsuario = :idMUsuario")})
public class HorarioEmp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "activo")
    private Boolean activo;
    @Column(name = "eliminado")
    private Boolean eliminado;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "fecha_reg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaReg;
    @Column(name = "fecha_venc")
    @Temporal(TemporalType.DATE)
    private Date fechaVenc;
    @Basic(optional = false)
    @Column(name = "id_m_usuario")
    private int idMUsuario;
    @JoinColumn(name = "viernes", referencedColumnName = "id")
    @ManyToOne
    private Horario viernes;
    @JoinColumn(name = "sabado", referencedColumnName = "id")
    @ManyToOne
    private Horario sabado;
    @JoinColumn(name = "miercoles", referencedColumnName = "id")
    @ManyToOne
    private Horario miercoles;
    @JoinColumn(name = "martes", referencedColumnName = "id")
    @ManyToOne
    private Horario martes;
    @JoinColumn(name = "lunes", referencedColumnName = "id")
    @ManyToOne
    private Horario lunes;
    @JoinColumn(name = "jueves", referencedColumnName = "id")
    @ManyToOne
    private Horario jueves;
    @JoinColumn(name = "domingo", referencedColumnName = "id")
    @ManyToOne
    private Horario domingo;
    @JoinColumn(name = "id_m_empleado", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empleado idMEmpleado;

    public HorarioEmp() {
    }

    public HorarioEmp(Integer id) {
        this.id = id;
    }

    public HorarioEmp(Integer id, int idMUsuario) {
        this.id = id;
        this.idMUsuario = idMUsuario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getEliminado() {
        return eliminado;
    }

    public void setEliminado(Boolean eliminado) {
        this.eliminado = eliminado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public Date getFechaVenc() {
        return fechaVenc;
    }

    public void setFechaVenc(Date fechaVenc) {
        this.fechaVenc = fechaVenc;
    }

    public int getIdMUsuario() {
        return idMUsuario;
    }

    public void setIdMUsuario(int idMUsuario) {
        this.idMUsuario = idMUsuario;
    }

    public Horario getViernes() {
        return viernes;
    }

    public void setViernes(Horario viernes) {
        this.viernes = viernes;
    }

    public Horario getSabado() {
        return sabado;
    }

    public void setSabado(Horario sabado) {
        this.sabado = sabado;
    }

    public Horario getMiercoles() {
        return miercoles;
    }

    public void setMiercoles(Horario miercoles) {
        this.miercoles = miercoles;
    }

    public Horario getMartes() {
        return martes;
    }

    public void setMartes(Horario martes) {
        this.martes = martes;
    }

    public Horario getLunes() {
        return lunes;
    }

    public void setLunes(Horario lunes) {
        this.lunes = lunes;
    }

    public Horario getJueves() {
        return jueves;
    }

    public void setJueves(Horario jueves) {
        this.jueves = jueves;
    }

    public Horario getDomingo() {
        return domingo;
    }

    public void setDomingo(Horario domingo) {
        this.domingo = domingo;
    }

    public Empleado getIdMEmpleado() {
        return idMEmpleado;
    }

    public void setIdMEmpleado(Empleado idMEmpleado) {
        this.idMEmpleado = idMEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorarioEmp)) {
            return false;
        }
        HorarioEmp other = (HorarioEmp) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "controldeasistencias.entidades.HorarioEmp[ id=" + id + " ]";
    }
    
}
