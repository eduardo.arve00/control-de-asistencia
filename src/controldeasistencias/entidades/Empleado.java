/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "empleado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e"),
    @NamedQuery(name = "Empleado.findById", query = "SELECT e FROM Empleado e WHERE e.id = :id"),
    @NamedQuery(name = "Empleado.findByCedula", query = "SELECT e FROM Empleado e WHERE e.cedula LIKE :cedula"),
    @NamedQuery(name = "Empleado.findByExactCedula", query = "SELECT e FROM Empleado e WHERE e.cedula = :cedula"),
    @NamedQuery(name = "Empleado.findByApellido", query = "SELECT e FROM Empleado e WHERE e.apellido LIKE :apellido"),
    @NamedQuery(name = "Empleado.findByNombres", query = "SELECT e FROM Empleado e WHERE e.nombres LIKE :nombres"),
    @NamedQuery(name = "Empleado.findByEstatus", query = "SELECT e FROM Empleado e WHERE e.estatus = :estatus"),
    @NamedQuery(name = "Empleado.findByEliminado", query = "SELECT e FROM Empleado e WHERE e.eliminado = :eliminado"),
    @NamedQuery(name = "Empleado.findByIdMUsuario", query = "SELECT e FROM Empleado e WHERE e.idMUsuario = :idMUsuario"),
    @NamedQuery(name = "Empleado.findByIdMCargo", query = "SELECT e FROM Empleado e WHERE e.idMCargo = :idMCargo"),
    @NamedQuery(name = "Empleado.findByFechaReg", query = "SELECT e FROM Empleado e WHERE e.fechaReg = :fechaReg"),
    @NamedQuery(name = "Empleado.findByFecha", query = "SELECT e FROM Empleado e WHERE e.fecha = :fecha"),
    @NamedQuery(name = "Empleado.findByDirHab", query = "SELECT e FROM Empleado e WHERE e.dirHab = :dirHab"),
    @NamedQuery(name = "Empleado.findByTelefono", query = "SELECT e FROM Empleado e WHERE e.telefono = :telefono"),
    @NamedQuery(name = "Empleado.findByFechaNac", query = "SELECT e FROM Empleado e WHERE e.fechaNac = :fechaNac"),
    @NamedQuery(name = "Empleado.findByEdoCivil", query = "SELECT e FROM Empleado e WHERE e.edoCivil = :edoCivil"),
    @NamedQuery(name = "Empleado.findByNacionalidad", query = "SELECT e FROM Empleado e WHERE e.nacionalidad = :nacionalidad"),
    @NamedQuery(name = "Empleado.findByFechaIngreso", query = "SELECT e FROM Empleado e WHERE e.fechaIngreso = :fechaIngreso"),
    @NamedQuery(name = "Empleado.findByIdMEmpleado", query = "SELECT e FROM Empleado e WHERE e.idMEmpleado = :idMEmpleado"),
    @NamedQuery(name = "Empleado.findByObservacion", query = "SELECT e FROM Empleado e WHERE e.observacion = :observacion"),
    @NamedQuery(name = "Empleado.findByModalidad", query = "SELECT e FROM Empleado e WHERE e.modalidad = :modalidad"),
    @NamedQuery(name = "Empleado.findByModalidadDes", query = "SELECT e FROM Empleado e WHERE e.modalidadDes = :modalidadDes")})
public class Empleado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "cedula")
    private String cedula;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "nombres")
    private String nombres;
    @Column(name = "estatus")
    private String estatus;
    @Column(name = "eliminado")
    private Boolean eliminado;
    @Lob
    @Column(name = "huella")
    private byte[] huella;
    @Column(name = "id_m_usuario")
    private Integer idMUsuario;

    @ManyToOne
    @JoinColumn(name = "id_m_cargo")
    private Cargo idMCargo;

    @ManyToOne
    @JoinColumn(name = "id_m_departamentos")
    private Departamento empl_dep;

    @Column(name = "fecha_reg")
    @Temporal(TemporalType.DATE)
    private Date fechaReg;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "dir_hab")
    private String dirHab;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "fecha_nac")
    @Temporal(TemporalType.DATE)
    private Date fechaNac;
    @Column(name = "edo_civil")
    private Integer edoCivil;
    @Column(name = "nacionalidad")
    private Integer nacionalidad;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.DATE)
    private Date fechaIngreso;
    @Basic(optional = false)
    @Column(name = "id_m_empleado")
    private int idMEmpleado;
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "modalidad")
    private String modalidad;
    @Column(name = "modalidad_des")
    private String modalidadDes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMEmpleado")
    private Collection<HorarioEmp> horarioEmpCollection;

    public Empleado() {
    }

    public Empleado(Integer id) {
        this.id = id;
    }

    public Empleado(Integer id, Cargo idMCargo, Departamento idMDepartamentos, int idMEmpleado) {
        this.id = id;
        this.idMCargo = idMCargo;
        this.empl_dep = idMDepartamentos;
        this.idMEmpleado = idMEmpleado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public Boolean getEliminado() {
        return eliminado;
    }

    public void setEliminado(Boolean eliminado) {
        this.eliminado = eliminado;
    }

    public byte[] getHuella() {
        return huella;
    }

    public void setHuella(byte[] huella) {
        this.huella = huella;
    }

    public Integer getIdMUsuario() {
        return idMUsuario;
    }

    public void setIdMUsuario(Integer idMUsuario) {
        this.idMUsuario = idMUsuario;
    }

    public Cargo getIdMCargo() {
        return idMCargo;
    }

    public void setIdMCargo(Cargo idMCargo) {
        this.idMCargo = idMCargo;
    }

    public Departamento getIdMDepartamentos() {
        return empl_dep;
    }

    public void setIdMDepartamentos(Departamento idMDepartamentos) {
        this.empl_dep = idMDepartamentos;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDirHab() {
        return dirHab;
    }

    public void setDirHab(String dirHab) {
        this.dirHab = dirHab;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public Integer getEdoCivil() {
        return edoCivil;
    }

    public void setEdoCivil(Integer edoCivil) {
        this.edoCivil = edoCivil;
    }

    public Integer getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Integer nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getIdMEmpleado() {
        return idMEmpleado;
    }

    public void setIdMEmpleado(int idMEmpleado) {
        this.idMEmpleado = idMEmpleado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getModalidadDes() {
        return modalidadDes;
    }

    public void setModalidadDes(String modalidadDes) {
        this.modalidadDes = modalidadDes;
    }

    @XmlTransient
    public Collection<HorarioEmp> getHorarioEmpCollection() {
        return horarioEmpCollection;
    }

    public void setHorarioEmpCollection(Collection<HorarioEmp> horarioEmpCollection) {
        this.horarioEmpCollection = horarioEmpCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "controldeasistencias.entidades.Empleado[ id=" + id + " ]";
    }
    
}
