/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "permisos_emp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PermisosEmp.findAll", query = "SELECT p FROM PermisosEmp p"),
    @NamedQuery(name = "PermisosEmp.findById", query = "SELECT p FROM PermisosEmp p WHERE p.id = :id"),
    @NamedQuery(name = "PermisosEmp.findByIdPermiso", query = "SELECT p FROM PermisosEmp p WHERE p.idPermiso = :idPermiso"),
    @NamedQuery(name = "PermisosEmp.findByIdEmpleado", query = "SELECT p FROM PermisosEmp p WHERE p.idEmpleado = :idEmpleado")})
public class PermisosEmp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_permiso")
    private Integer idPermiso;
    @Column(name = "id_empleado")
    private Integer idEmpleado;
    
    @Temporal(TemporalType.DATE)
    @Column(name ="fecha")
    private Date fecha; 

    public PermisosEmp() {
    }

    public PermisosEmp(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPermiso() {
        return idPermiso;
    }

    public void setIdPermiso(Integer idPermiso) {
        this.idPermiso = idPermiso;
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermisosEmp)) {
            return false;
        }
        PermisosEmp other = (PermisosEmp) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "controldeasistencias.entidades.PermisosEmp[ id=" + id + " ]";
    }
    
}
