/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "asistencia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asistencia.findAll", query = "SELECT a FROM Asistencia a"),
    @NamedQuery(name = "Asistencia.findById", query = "SELECT a FROM Asistencia a WHERE a.id = :id"),
    @NamedQuery(name = "Asistencia.findByIdEmpleado", query = "SELECT a FROM Asistencia a WHERE a.idEmpleado = :idEmpleado"),
    @NamedQuery(name = "Asistencia.findByEstatus", query = "SELECT a FROM Asistencia a WHERE a.estatus = :estatus"),
    @NamedQuery(name = "Asistencia.findByNombreCompleto", query = "SELECT a FROM Asistencia a WHERE a.nombreCompleto LIKE :nombreCompleto"),
    @NamedQuery(name = "Asistencia.findByTipo", query = "SELECT a FROM Asistencia a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "Asistencia.findByCedula", query = "SELECT a FROM Asistencia a WHERE a.cedula LIKE :cedula"),
    @NamedQuery(name = "Asistencia.findByFecha", query = "SELECT a FROM Asistencia a WHERE a.fecha = :fecha"),
    @NamedQuery(name = "Asistencia.findByFechaYCedula", query = "SELECT a FROM Asistencia a WHERE a.fecha = :fecha and a.cedula LIKE :cedula"),
    @NamedQuery(name = "Asistencia.findByFechaYNombre", query = "SELECT a FROM Asistencia a WHERE a.fecha = :fecha and a.nombreCompleto LIKE :nombreCompleto"),
    @NamedQuery(name = "Asistencia.findByHoraSal", query = "SELECT a FROM Asistencia a WHERE a.horaSal = :horaSal"),
    @NamedQuery(name = "Asistencia.findByHoraIng", query = "SELECT a FROM Asistencia a WHERE a.horaIng = :horaIng"),
    @NamedQuery(name = "Asistencia.findByComentario", query = "SELECT a FROM Asistencia a WHERE a.comentario = :comentario"),
    //@NamedQuery(name = "Asistencia.findByCurrentDay", query = "SELECT a FROM Asistencia a WHERE YEAR(:yourdate) = YEAR(a.horaSal) AND MONTH(:yourdate) = MONTH(a.horaSal) and DAY(:yourdate) = DATE(a.horaSal)"),
    @NamedQuery(name = "Asistencia.findByIdMHorario", query = "SELECT a FROM Asistencia a WHERE a.idMHorario = :idMHorario")})
public class Asistencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)

    @Column(name = "id_empleado")
    private int idEmpleado;

    @Column(name = "estatus")
    private String estatus;
    @Column(name = "nombre_completo")
    private String nombreCompleto;
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "cedula")
    private String cedula;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "hora_sal")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horaSal;
    @Column(name = "hora_ing")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horaIng;
    @Column(name = "comentario")
    private String comentario;
    @Basic(optional = false)
    @Column(name = "id_m_horario")
    private int idMHorario;
    
        @Column(name = "min_extras")
    private int minutosExtras;
    @Column(name = "min_retrazo")
    private int minutosRetrazo;

    public Asistencia() {
    }

    public Asistencia(Integer id) {
        this.id = id;
    }

    public Asistencia(Integer id, int idEmpleado, int idMHorario) {
        this.id = id;
        this.idEmpleado = idEmpleado;
        this.idMHorario = idMHorario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public int getMinutosExtras() {
        return minutosExtras;
    }

    public void setMinutosExtras(int minutosExtras) {
        this.minutosExtras = minutosExtras;
    }

    public int getMinutosRetrazo() {
        return minutosRetrazo;
    }

    public void setMinutosRetrazo(int minutosRetrazo) {
        this.minutosRetrazo = minutosRetrazo;
    }
    
    

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHoraSal() {
        return horaSal;
    }

    public void setHoraSal(Date horaSal) {
        this.horaSal = horaSal;
    }

    public Date getHoraIng() {
        return horaIng;
    }

    public void setHoraIng(Date horaIng) {
        this.horaIng = horaIng;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getIdMHorario() {
        return idMHorario;
    }

    public void setIdMHorario(int idMHorario) {
        this.idMHorario = idMHorario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asistencia)) {
            return false;
        }
        Asistencia other = (Asistencia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "controldeasistencias.entidades.Asistencia[ id=" + id + " ]";
    }

}
