/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.controllers;

import controldeasistencias.controllers.exceptions.NonexistentEntityException;
import controldeasistencias.entidades.PermisosEmp;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Administrador
 */
public class PermisosEmpJpaController implements Serializable {

    public PermisosEmpJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PermisosEmp permisosEmp) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(permisosEmp);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PermisosEmp permisosEmp) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            permisosEmp = em.merge(permisosEmp);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = permisosEmp.getId();
                if (findPermisosEmp(id) == null) {
                    throw new NonexistentEntityException("The permisosEmp with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PermisosEmp permisosEmp;
            try {
                permisosEmp = em.getReference(PermisosEmp.class, id);
                permisosEmp.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The permisosEmp with id " + id + " no longer exists.", enfe);
            }
            em.remove(permisosEmp);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PermisosEmp> findPermisosEmpEntities() {
        return findPermisosEmpEntities(true, -1, -1);
    }

    public List<PermisosEmp> findPermisosEmpEntities(int maxResults, int firstResult) {
        return findPermisosEmpEntities(false, maxResults, firstResult);
    }

    private List<PermisosEmp> findPermisosEmpEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PermisosEmp.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PermisosEmp findPermisosEmp(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PermisosEmp.class, id);
        } finally {
            em.close();
        }
    }

    public int getPermisosEmpCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PermisosEmp> rt = cq.from(PermisosEmp.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
