/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griaule.fingerprintsdk;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 *
 * @author Administrador
 */
public class TextFingerView implements IViewFinger {
    
     //object used to perform all the Fingerprint SDK-related operations
    private UtilSDK fingerprintSDK;


    public TextFingerView(int idEmpleado) {
        
        //Creates an "Util" instance.
        this.fingerprintSDK = new UtilSDK(this, idEmpleado);
    }

    
    @Override
    public void setColorLog(Color bgLog) {

    }

    @Override
    public void writeLog(String text) {
        System.out.println("LOG: " + text);
    }

    @Override
    public void showImage(BufferedImage image) {
        
    }

    @Override
    public void enableImage() {
        
    }

    @Override
    public void enableTemplate() {
        
    }

}
