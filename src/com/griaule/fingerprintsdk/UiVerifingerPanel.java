/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griaule.fingerprintsdk;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Administrador
 */
public class UiVerifingerPanel extends javax.swing.JPanel implements IViewFinger {

    //object used to perform all the Fingerprint SDK-related operations
    private UtilSDK fingerprintSDK;

    //The image of the current fingerprint.
    private BufferedImage fingerprintImage = null;

    private int idEmpleado;

    /**
     * Creates new form UiVerifingerPanel
     */
    public UiVerifingerPanel(int id) {
        this.idEmpleado = id;
        initComponents();
        myInitComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fingerprintViewPanel = new javax.swing.JPanel();
        logScrollPane = new javax.swing.JScrollPane();
        logTextArea = new javax.swing.JTextArea();

        fingerprintViewPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout fingerprintViewPanelLayout = new javax.swing.GroupLayout(fingerprintViewPanel);
        fingerprintViewPanel.setLayout(fingerprintViewPanelLayout);
        fingerprintViewPanelLayout.setHorizontalGroup(
            fingerprintViewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        fingerprintViewPanelLayout.setVerticalGroup(
            fingerprintViewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 313, Short.MAX_VALUE)
        );

        logTextArea.setColumns(20);
        logTextArea.setRows(5);
        logScrollPane.setViewportView(logTextArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(fingerprintViewPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(logScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 292, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fingerprintViewPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel fingerprintViewPanel;
    private javax.swing.JScrollPane logScrollPane;
    private javax.swing.JTextArea logTextArea;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setColorLog(Color bgLog) {
        logTextArea.setBackground(bgLog);

    }

    @Override
    public void writeLog(String text) {
        //Appends the text
        logTextArea.setText(text);

        //Auto-scrolls to the last message.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //picks the vertical scrollBar, and sets it to the maximum.
                JScrollBar vbar = logScrollPane.getVerticalScrollBar();
                vbar.setValue(vbar.getMaximum());
            }
        });
    }

    @Override
    public void showImage(BufferedImage image) {
        //uses the imageProducer to create the fingerprint image
        fingerprintImage = image;
        //Repaint, so that the new image is shown.
        repaint();
    }

    @Override
    public void enableImage() {
    }

    @Override
    public void enableTemplate() {
    }

    /**
     * Frees Fingerprint SDK resources and finished the program.
     */
    public void destroy() {
        if (fingerprintSDK != null) {
            fingerprintSDK.destroy();
        }
    }

    private void myInitComponents() {
        //Creates the textArea where the log will be written
        logTextArea = new JTextArea();
        logTextArea.setEditable(false); //It's not editable
        logTextArea.setLineWrap(true);  //Enabled wrapping
        logTextArea.setFont(Font.decode("arial-11")); //Sets the font

        //Creates a ScrollPane, so that we can scroll down the log when it's too big
        logScrollPane = new JScrollPane(logTextArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        //Sets it's size
        logScrollPane.setPreferredSize(new java.awt.Dimension(0, 60));
        //Enables auto-scrolling
        logScrollPane.setAutoscrolls(true);

        //Adds a little border around it
        logScrollPane.setBorder(new CompoundBorder(
                new EmptyBorder(2, 2, 2, 2),
                new BevelBorder(BevelBorder.LOWERED)));

        //Creates an "Util" instance.
        this.fingerprintSDK = new UtilSDK(this, idEmpleado);
        this.fingerprintSDK.setAutoIdentify(true);

        fingerprintViewPanel = new JPanel() {//Overrides the paintComponent method for painting the image
            public void paintComponent(Graphics g) {
                super.paintComponent(g);

                //If there's a image to be drawn...
                if (fingerprintImage != null) {
                    //calculates the size/position of the image being drawn,
                    //so it's size is stretched to fill the whole space
                    Insets insets = getInsets();
                    int transX = insets.left;
                    int transY = insets.top;
                    int width = getWidth() - getInsets().right - getInsets().left;
                    int height = getHeight() - getInsets().bottom - getInsets().top;

                    //draw it!
                    g.drawImage(fingerprintImage, transX, transY, width, height, null);
                }

            }
        };

        //Creates an "Util" instance.
        this.fingerprintSDK = new UtilSDK(this, idEmpleado);
    }

    public void verificar() {
        fingerprintSDK.verify(idEmpleado);
    }
}
